# Webflux + R2DBC + QueryDSL

Simple boiler-plate application using

* R2DBC
* Flyway
* QueryDSL
* Spring boot
* Swagger

## TLDR;

* Build:
```bash
./gradlew clean build
```
* Test:
```bash
./gradlew test
```
* Run
```bash
./gradle bootJar && docker-compose up --build 
```

## Useful endpoints

* `/live` - Liveness check
* `/ready` - Readiness check
* `/documentation` - Swagger documentation
