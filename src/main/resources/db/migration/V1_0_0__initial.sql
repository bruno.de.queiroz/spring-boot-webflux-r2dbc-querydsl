CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE auth
(
    id            uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    email         varchar(100)     NOT NULL UNIQUE,
    password      varchar(255)     NOT NULL,
    password_hash varchar(255)     NOT NULL,
    roles         text[],
    created_at    timestamp        NOT NULL DEFAULT now(),
    updated_at    timestamp        NOT NULL DEFAULT now(),
    deleted_at    timestamp,
    verified_at   timestamp
);

CREATE TABLE customer
(
    id                  uuid PRIMARY KEY NOT NULL REFERENCES auth (id) ON DELETE CASCADE,
    first_name          varchar(100)     NOT NULL,
    last_name           varchar(100)     NOT NULL,
    birth_date          date             NOT NULL,
    phone_number        varchar(20)      NOT NULL,
    terms_acceptance_at timestamp        NOT NULL DEFAULT now(),
    created_at          timestamp        NOT NULL DEFAULT now(),
    updated_at          timestamp        NOT NULL DEFAULT now(),
    deleted_at          timestamp,
    CONSTRAINT check_first_name_min_length check (length(first_name) >= 3),
    CONSTRAINT check_last_name_min_length check (length(last_name) >= 3),
    CONSTRAINT check_phone_number_min_length check (length(phone_number) >= 10)
);

CREATE TABLE address
(
    id           uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    user_id      uuid             NOT NULL REFERENCES auth (id) ON DELETE CASCADE,
    alias        varchar(20)      NOT NULL,
    first_name   varchar(100),
    last_name    varchar(100),
    phone_number varchar(20),
    line_1       varchar(255)     NOT NULL,
    line_2       varchar(255),
    company_name varchar(255),
    postal_code  varchar(20)      NOT NULL,
    latitude     double precision NOT NULL,
    longitude    double precision NOT NULL,
    city         varchar(100)     NOT NULL,
    district     varchar(100)     NOT NULL,
    country      varchar(100)     NOT NULL,
    created_at   timestamp        NOT NULL DEFAULT now(),
    updated_at   timestamp        NOT NULL DEFAULT now(),
    deleted_at   timestamp,
    CONSTRAINT unique_user_id_alias UNIQUE (user_id, alias),
    CONSTRAINT check_alias_min_length check (length(alias) >= 4),
    CONSTRAINT check_line_1_min_length check (length(line_1) >= 5),
    CONSTRAINT check_postal_code_min_length check (length(postal_code) >= 3),
    CONSTRAINT check_city_min_length check (length(city) >= 3),
    CONSTRAINT check_district_min_length check (length(district) >= 3),
    CONSTRAINT check_country_min_length check (length(country) >= 3)
);

CREATE TABLE tag
(
    id          uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    name        varchar(100)     NOT NULL,
    slug        varchar(100)     NOT NULL UNIQUE,
    description varchar(255)     NOT NULL,
    created_at  timestamp        NOT NULL DEFAULT now(),
    updated_at  timestamp        NOT NULL DEFAULT now(),
    deleted_at  timestamp        NULL,
    CONSTRAINT check_name_min_length check (length(name) >= 4),
    CONSTRAINT check_slug_min_length check (length(slug) >= 4)
);

CREATE TABLE product
(
    id          uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    slug        varchar(100)     NOT NULL UNIQUE,
    name        varchar(100)     NOT NULL,
    description varchar(255)     NOT NULL,
    price       double precision NOT NULL,
    value       bigint           NOT NULL,
    unit        varchar(100)     NOT NULL,
    quantity    bigint           NOT NULL,
    tags        uuid[],
    created_at  timestamp        NOT NULL DEFAULT now(),
    updated_at  timestamp        NOT NULL DEFAULT now(),
    deleted_at  timestamp        NULL,
    CONSTRAINT check_name_min_length check (length(name) >= 4),
    CONSTRAINT check_slug_min_length check (length(slug) >= 4)
);
