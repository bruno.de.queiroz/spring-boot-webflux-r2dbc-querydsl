package com.example.querydsl

import com.infobip.spring.data.r2dbc.QuerydslR2dbcRepositoryFactoryBean
import com.querydsl.sql.SQLQueryFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.r2dbc.core.R2dbcEntityOperations
import org.springframework.data.repository.Repository
import org.springframework.data.repository.core.support.RepositoryFactorySupport
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.transaction.ReactiveTransactionManager
import java.io.Serializable

open class SoftDeleteFragmentFactoryBean<T : Repository<S, ID>, S, ID : Serializable>(repositoryInterface: Class<out T>) :
    QuerydslR2dbcRepositoryFactoryBean<T, S, ID>(repositoryInterface) {

    private lateinit var databaseClient: DatabaseClient
    private lateinit var reactiveTransactionManager: ReactiveTransactionManager

    @Autowired
    private lateinit var sqlQueryFactory: SQLQueryFactory

    @Autowired
    override fun setReactiveTransactionManager(reactiveTransactionManager: ReactiveTransactionManager) {
        super.setReactiveTransactionManager(reactiveTransactionManager)
        this.reactiveTransactionManager = reactiveTransactionManager
    }

    @Autowired
    override fun setDatabaseClient(databaseClient: DatabaseClient) {
        super.setDatabaseClient(databaseClient)
        this.databaseClient = databaseClient
    }

    override fun getFactoryInstance(operations: R2dbcEntityOperations): RepositoryFactorySupport {
        return SoftDeleteFragmentFactory(operations, sqlQueryFactory, reactiveTransactionManager, databaseClient)
    }
}
