package com.example.querydsl

import com.example.api.domain.SoftDeletable
import reactor.core.publisher.Mono

interface SoftDeleteFragment<MODEL : SoftDeletable<ID>, ID> {
    fun findActive(id: ID): Mono<MODEL>
    fun deactivate(id: ID): Mono<Void>
}
