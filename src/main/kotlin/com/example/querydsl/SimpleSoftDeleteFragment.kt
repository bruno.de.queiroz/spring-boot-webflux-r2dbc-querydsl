package com.example.querydsl

import com.example.api.domain.SoftDeletable
import com.querydsl.core.types.ConstructorExpression
import com.querydsl.core.types.ExpressionUtils
import com.querydsl.core.types.dsl.Expressions
import com.querydsl.sql.RelationalPath
import com.querydsl.sql.SQLQuery
import com.querydsl.sql.SQLQueryFactory
import com.querydsl.sql.dml.SQLUpdateClause
import org.slf4j.LoggerFactory
import org.springframework.data.r2dbc.convert.EntityRowMapper
import org.springframework.data.r2dbc.convert.R2dbcConverter
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.r2dbc.core.RowsFetchSpec
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.reactive.TransactionalOperator
import reactor.core.publisher.Mono
import java.io.Serializable
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class SimpleSoftDeleteFragment<MODEL : SoftDeletable<ID>, ID : Serializable>(
    private val sqlQueryFactory: SQLQueryFactory,
    private val constructorExpression: ConstructorExpression<MODEL>,
    path: RelationalPath<*>,
    private val reactiveTransactionManager: ReactiveTransactionManager,
    private val databaseClient: DatabaseClient,
    private val converter: R2dbcConverter
) : SoftDeleteFragment<MODEL, ID> {

    companion object {
        private val log = LoggerFactory.getLogger(this::class.java)
    }

    @Suppress("unchecked_cast")
    private val path = path as RelationalPath<MODEL>

    override fun findActive(id: ID): Mono<MODEL> {
        val sqlQuery: SQLQuery<MODEL> = sqlQueryFactory.query()
            .select(constructorExpression)
            .where(
                ExpressionUtils.eq(Expressions.stringPath("id"), Expressions.asString(id.toString())),
                ExpressionUtils.isNull(Expressions.stringPath("deleted_at"))
            )
            .from(path)
        return query(sqlQuery).one()
    }

    override fun deactivate(id: ID): Mono<Void> {
        val clause: SQLUpdateClause = sqlQueryFactory.update(path)
            .set(listOf(Expressions.stringPath("deleted_at")), listOf(OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)))
            .where(ExpressionUtils.eq(Expressions.stringPath("id"), Expressions.asString(id.toString())))
            .apply { setUseLiterals(true) }
        return databaseClient.sql(clause.toString())
            .fetch()
            .rowsUpdated()
            .doOnError { log.error("error: {}", it) }
            .doOnNext { log.debug("updated rowS: {}", it) }
            .`as` { mono -> TransactionalOperator.create(reactiveTransactionManager).transactional(mono) }
            .then()
    }

    private fun <M : MODEL> query(query: SQLQuery<M>): RowsFetchSpec<M> {
        query.setUseLiterals(true)
        val sql = query.sql.sql
        val mapper = EntityRowMapper(query.type, converter)
        return databaseClient.sql(sql).map(mapper)
    }
}
