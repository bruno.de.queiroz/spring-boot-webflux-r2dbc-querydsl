package com.example.querydsl

import com.infobip.spring.data.common.Querydsl
import com.infobip.spring.data.common.QuerydslExpressionFactory
import com.infobip.spring.data.r2dbc.QuerydslR2dbcFragment
import com.infobip.spring.data.r2dbc.ReactiveQuerydslR2dbcPredicateExecutor
import com.infobip.spring.data.r2dbc.SimpleQuerydslR2dbcFragment
import com.querydsl.core.types.ConstructorExpression
import com.querydsl.core.types.dsl.PathBuilder
import com.querydsl.sql.RelationalPath
import com.querydsl.sql.RelationalPathBase
import com.querydsl.sql.SQLQueryFactory
import org.springframework.data.r2dbc.core.R2dbcEntityOperations
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory
import org.springframework.data.repository.core.RepositoryMetadata
import org.springframework.data.repository.core.support.RepositoryComposition
import org.springframework.data.repository.core.support.RepositoryFragment
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.transaction.ReactiveTransactionManager

class SoftDeleteFragmentFactory(
    operations: R2dbcEntityOperations,
    private val sqlQueryFactory: SQLQueryFactory,
    private val reactiveTransactionManager: ReactiveTransactionManager,
    private val databaseClient: DatabaseClient
) : R2dbcRepositoryFactory(operations) {

    companion object {
        private val REPOSITORY_TARGET_TYPE: Array<Class<*>> = arrayOf(
            SoftDeleteFragment::class.java,
            QuerydslR2dbcFragment::class.java
        )
    }

    private val converter = operations.converter

    override fun getRepositoryFragments(metadata: RepositoryMetadata): RepositoryComposition.RepositoryFragments {
        val fragments = super.getRepositoryFragments(metadata)
        val repositoryInterface = metadata.repositoryInterface
        val assignable = REPOSITORY_TARGET_TYPE.firstOrNull { it.isAssignableFrom(repositoryInterface) } ?: return fragments
        val querydslExpressionFactory = QuerydslExpressionFactory(assignable)
        val path = querydslExpressionFactory.getRelationalPathBaseFromQueryRepositoryClass(repositoryInterface)
        val type = metadata.domainType
        val constructorExpression = querydslExpressionFactory.getConstructorExpression(type, path)
        val simpleSoftDeleteFragment = createSimpleSoftDeleteFragment(path, constructorExpression)
        val simpleQuerydslR2dbcFragment = createSimpleQuerydslR2dbcFragment(path, constructorExpression)
        val querydslJdbcPredicateExecutor = createQuerydslJdbcPredicateExecutor(path, constructorExpression)
        return fragments.append(simpleSoftDeleteFragment)
            .append(simpleQuerydslR2dbcFragment)
            .append(querydslJdbcPredicateExecutor)
    }

    private fun createSimpleSoftDeleteFragment(path: RelationalPath<*>, constructor: ConstructorExpression<*>): RepositoryFragment<Any> {
        return RepositoryFragment.implemented(
            getTargetRepositoryViaReflection<Any>(
                SimpleSoftDeleteFragment::class.java,
                sqlQueryFactory,
                constructor,
                path,
                reactiveTransactionManager,
                databaseClient,
                converter
            )
        )
    }

    private fun createSimpleQuerydslR2dbcFragment(path: RelationalPath<*>, constructor: ConstructorExpression<*>): RepositoryFragment<Any> {
        val simpleJPAQuerydslFragment = getTargetRepositoryViaReflection<Any>(
            SimpleQuerydslR2dbcFragment::class.java,
            sqlQueryFactory,
            constructor,
            path,
            reactiveTransactionManager,
            databaseClient,
            converter
        )
        return RepositoryFragment.implemented(simpleJPAQuerydslFragment)
    }

    private fun createQuerydslJdbcPredicateExecutor(path: RelationalPathBase<*>, constructorExpression: ConstructorExpression<*>): RepositoryFragment<Any> {
        val querydsl = Querydsl(sqlQueryFactory, PathBuilder(path.type, path.metadata))
        val querydslJdbcPredicateExecutor = getTargetRepositoryViaReflection<Any>(
            ReactiveQuerydslR2dbcPredicateExecutor::class.java,
            constructorExpression,
            path,
            sqlQueryFactory,
            querydsl,
            databaseClient,
            converter
        )
        return RepositoryFragment.implemented(querydslJdbcPredicateExecutor)
    }
}
