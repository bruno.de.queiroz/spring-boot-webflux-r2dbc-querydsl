package com.example.api.exception

import org.springframework.boot.web.error.ErrorAttributeOptions
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest

@Component
class GlobalErrorAttributes<T : Throwable>(val errorResponseComposer: ErrorResponseComposer<T>) : DefaultErrorAttributes() {

    override fun getErrorAttributes(request: ServerRequest, options: ErrorAttributeOptions): Map<String, Any> {
        val errorAttributes = super.getErrorAttributes(request, options)
        addErrorDetails(errorAttributes, request)
        return errorAttributes
    }

    protected fun addErrorDetails(errorAttributes: MutableMap<String, Any>, request: ServerRequest) {
        val ex: Throwable = getError(request)
        val requestId = request.exchange().request.id
        errorAttributes["request_id"] = requestId
        errorResponseComposer.compose(requestId, ex)?.let { errorResponse: ErrorResponse ->
            errorResponse.status?.let { errorAttributes["status"] = it }
            errorResponse.errors.takeIf { it.isNotEmpty() }?.let { errorAttributes["errors"] = it }
        }
    }
}
