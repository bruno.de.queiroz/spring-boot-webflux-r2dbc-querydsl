package com.example.api.exception

import org.springframework.http.HttpStatus

abstract class AbstractExceptionHandler<T : Throwable>(val exceptionClass: Class<T>) {

    open fun getStatus(ex: T): HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR

    open fun getErrors(ex: T): Collection<ErrorResponse.ErrorItem> = emptyList()

    fun getErrorResponse(requestId: String, ex: T): ErrorResponse =
        ErrorResponse(
            requestId = requestId,
            status = getStatus(ex).value(),
            errors = getErrors(ex)
        )
}
