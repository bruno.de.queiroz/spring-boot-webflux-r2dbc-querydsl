package com.example.api.exception

enum class ErrorCode {
    CLIENT_ERROR,
    SERVER_ERROR,
    INTEGRITY_VIOLATION,
    VALIDATION_ERROR
}
