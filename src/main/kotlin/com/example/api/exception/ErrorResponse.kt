package com.example.api.exception

data class ErrorResponse(
    val requestId: String,
    val status: Int?,
    val errors: Collection<ErrorItem> = emptyList()
) {

    data class ErrorItem(
        val code: ErrorCode,
        val message: String? = null,
        val payload: Map<String, String> = emptyMap()
    ) {
        companion object {
            fun error(code: ErrorCode, message: String?) =
                ErrorItem(code, message)

            fun fieldError(code: ErrorCode, message: String?, field: String) =
                ErrorItem(code, message, mapOf("field" to field))
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ErrorItem

            if (code != other.code) return false
            if (message != other.message) return false
            if (payload != other.payload) return false

            return true
        }

        override fun hashCode(): Int {
            var result = code.hashCode()
            result = 31 * result + (message?.hashCode() ?: 0)
            result = 31 * result + payload.hashCode()
            return result
        }
    }
}
