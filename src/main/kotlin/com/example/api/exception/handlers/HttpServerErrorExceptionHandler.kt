package com.example.api.exception.handlers

import com.example.api.exception.AbstractExceptionHandler
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpServerErrorException

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
class HttpServerErrorExceptionHandler : AbstractExceptionHandler<HttpServerErrorException>(HttpServerErrorException::class.java) {
    override fun getStatus(ex: HttpServerErrorException): HttpStatus = ex.statusCode
    override fun getErrors(ex: HttpServerErrorException): Collection<ErrorResponse.ErrorItem> {
        return listOf(ErrorResponse.ErrorItem.error(ErrorCode.SERVER_ERROR, ex.statusText))
    }
}
