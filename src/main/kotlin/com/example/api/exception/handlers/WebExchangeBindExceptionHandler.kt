package com.example.api.exception.handlers

import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.support.WebExchangeBindException

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
class WebExchangeBindExceptionHandler :
    AbstractValidationExceptionHandler<WebExchangeBindException>(WebExchangeBindException::class.java) {
    override fun getStatus(ex: WebExchangeBindException): HttpStatus = HttpStatus.BAD_REQUEST
    override fun getErrors(ex: WebExchangeBindException): Collection<ErrorResponse.ErrorItem> =
        ex.bindingResult.fieldErrors
            .map {
                ErrorResponse.ErrorItem.fieldError(
                    ErrorCode.VALIDATION_ERROR,
                    it.defaultMessage,
                    it.field
                )
            }
}
