package com.example.api.exception.handlers

import com.example.api.exception.AbstractExceptionHandler
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus

@Order(Ordered.LOWEST_PRECEDENCE)
abstract class AbstractValidationExceptionHandler<T : Throwable>(exceptionClass: Class<T>) : AbstractExceptionHandler<T>(exceptionClass) {
    override fun getStatus(ex: T): HttpStatus = HttpStatus.UNPROCESSABLE_ENTITY
    override fun getErrors(ex: T): Collection<ErrorResponse.ErrorItem> = listOf(ErrorResponse.ErrorItem(ErrorCode.VALIDATION_ERROR))
}
