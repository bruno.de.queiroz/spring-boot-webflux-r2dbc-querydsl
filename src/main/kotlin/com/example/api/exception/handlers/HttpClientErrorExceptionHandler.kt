package com.example.api.exception.handlers

import com.example.api.exception.AbstractExceptionHandler
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
class HttpClientErrorExceptionHandler : AbstractExceptionHandler<HttpClientErrorException>(HttpClientErrorException::class.java) {
    override fun getStatus(ex: HttpClientErrorException): HttpStatus = ex.statusCode
    override fun getErrors(ex: HttpClientErrorException): Collection<ErrorResponse.ErrorItem> {
        return listOf(ErrorResponse.ErrorItem.error(ErrorCode.CLIENT_ERROR, ex.statusText))
    }
}
