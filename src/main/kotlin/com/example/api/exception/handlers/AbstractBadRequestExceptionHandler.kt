package com.example.api.exception.handlers

import com.example.api.exception.AbstractExceptionHandler
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus

@Order(Ordered.LOWEST_PRECEDENCE)
abstract class AbstractBadRequestExceptionHandler<T : Throwable>(exceptionClass: Class<T>) : AbstractExceptionHandler<T>(exceptionClass) {
    override fun getStatus(ex: T): HttpStatus = HttpStatus.BAD_REQUEST
}
