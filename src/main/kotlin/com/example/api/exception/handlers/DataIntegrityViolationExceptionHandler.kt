package com.example.api.exception.handlers

import com.example.api.exception.AbstractExceptionHandler
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
class DataIntegrityViolationExceptionHandler : AbstractExceptionHandler<DataIntegrityViolationException>(DataIntegrityViolationException::class.java) {
    companion object {
        private val log = LoggerFactory.getLogger(this::class.java)
        val checkConstraintRegex = ".*violation check.*".toRegex()
    }

    override fun getStatus(ex: DataIntegrityViolationException): HttpStatus =
        when {
            (ex.message ?: "").matches(checkConstraintRegex) -> HttpStatus.BAD_REQUEST
            else -> HttpStatus.CONFLICT
        }

    override fun getErrors(ex: DataIntegrityViolationException): Collection<ErrorResponse.ErrorItem> {
        log.error("{}", ex)
        return listOf(ErrorResponse.ErrorItem.error(ErrorCode.INTEGRITY_VIOLATION, "There was a conflict with the data sent"))
    }
}
