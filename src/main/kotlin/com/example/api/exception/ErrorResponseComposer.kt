package com.example.api.exception

import org.springframework.core.annotation.AnnotationAwareOrderComparator
import org.springframework.stereotype.Component
import kotlin.reflect.jvm.javaType

@Component
class ErrorResponseComposer<T : Throwable>(handlers: List<AbstractExceptionHandler<T>>) {

    private val handlers: Map<String, AbstractExceptionHandler<T>> =
        handlers.map { it.exceptionClass.typeName to it }
            .groupBy { it.first }
            .mapValues { it.value.map { item -> item.second } }
            .mapValues { it.value.sortedWith(AnnotationAwareOrderComparator.INSTANCE).first() }

    @Suppress("UNCHECKED_CAST")
    fun compose(requestId: String, ex: Throwable): ErrorResponse? =
        getHandler(ex)?.getErrorResponse(requestId, ex as T)

    private fun getHandler(ex: Throwable) =
        (listOf(ex::class.java.typeName) + ex::class.supertypes.map { it.javaType.typeName })
            .mapNotNull { handlers[it] }
            .firstOrNull()
}
