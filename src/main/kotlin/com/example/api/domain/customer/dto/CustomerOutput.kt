package com.example.api.domain.customer.dto

import java.time.LocalDate
import java.util.UUID

data class CustomerOutput(
    val id: UUID,
    val firstName: String,
    val lastName: String,
    val birthDate: LocalDate,
    val phoneNumber: String
)
