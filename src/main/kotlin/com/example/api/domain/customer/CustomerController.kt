package com.example.api.domain.customer

import com.example.api.domain.IdOutput
import com.example.api.domain.customer.dto.CustomerInput
import com.example.api.domain.customer.dto.CustomerOutput
import com.example.api.domain.customer.dto.CustomerSearchCriteria
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.notFoundIfEmpty
import com.example.api.extensions.toUUID
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/api/v1/customers", produces = [MediaType.APPLICATION_JSON_VALUE])
class CustomerController(val mapper: CustomerMapper, val service: CustomerService) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('ADMIN')")
    fun search(@Valid criteria: CustomerSearchCriteria, page: Pageable): Flux<CustomerOutput> =
        service.findByCriteria(
            text = criteria.text,
            ids = criteria.ids,
            page = page
        ).map { mapper.toOutput(it) }

    @Operation(summary = "Create customer", description = "Returns the id of the customer created")
    @ApiResponses(
        ApiResponse(responseCode = "202", description = "Created with success"),
        ApiResponse(responseCode = "400", description = "Bad body parameters", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "409", description = "Conflict creating the customer", content = [Content(schema = Schema(implementation = ErrorResponse::class))])
    )
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('CUSTOMER')")
    fun create(
        @AuthenticationPrincipal jwt: Jwt,
        @RequestBody @Valid input: CustomerInput
    ): Mono<IdOutput<UUID>> =
        Mono.just(input)
            .map { mapper.toTransient(it) }
            .map { it.copy(id = jwt.subject.toUUID()) }
            .flatMap { service.create(it) }
            .map { IdOutput(it) }

    @Operation(summary = "Fetch customer by id", description = "Returns the customer based on the id")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Resource found with success"),
        ApiResponse(responseCode = "404", description = "Resource not found", content = [Content(schema = Schema(implementation = ErrorResponse::class))])
    )
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #id.toString()) or hasAuthority('ADMIN')")
    fun fetch(@PathVariable id: UUID): Mono<CustomerOutput> =
        service.findById(id)
            .map { mapper.toOutput(it) }
            .notFoundIfEmpty("Customer not found")

    @Operation(summary = "Update a customer by id", description = "Returns the customer id")
    @ApiResponses(
        ApiResponse(responseCode = "202", description = "Resource updated with success"),
        ApiResponse(responseCode = "400", description = "Bad body parameters", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", description = "Resource not found", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "409", description = "Resource could not be update due conflict", content = [Content(schema = Schema(implementation = ErrorResponse::class))])
    )
    @PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #id.toString()) or hasAuthority('ADMIN')")
    fun update(@PathVariable id: UUID, @RequestBody @Valid input: CustomerInput): Mono<IdOutput<UUID>> =
        Mono.just(input)
            .map { mapper.toTransient(it) }
            .flatMap { service.update(id, it) }
            .map { IdOutput(it) }
            .notFoundIfEmpty("Customer not found")

    @Operation(summary = "Delete a customer by id", description = "Returns the no content")
    @ApiResponses(
        ApiResponse(responseCode = "204", description = "Resource deleted with success"),
        ApiResponse(responseCode = "404", description = "Resource not found")
    )
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #id.toString()) or hasAuthority('ADMIN')")
    fun delete(@PathVariable id: UUID): Mono<Void> =
        service.findById(id)
            .notFoundIfEmpty("Customer not found")
            .flatMap { service.delete(it) }
}
