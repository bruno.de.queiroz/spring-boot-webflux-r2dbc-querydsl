package com.example.api.domain.customer

import com.example.api.domain.CrudRepository
import com.example.api.domain.customer.dto.Customer
import org.springframework.validation.annotation.Validated
import java.util.UUID

@Validated
interface CustomerRepository : CrudRepository<Customer, UUID>
