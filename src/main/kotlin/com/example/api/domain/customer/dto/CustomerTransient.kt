package com.example.api.domain.customer.dto

import com.example.api.domain.Identifiable
import java.time.Instant
import java.time.LocalDate
import java.util.UUID

data class CustomerTransient(
    override val id: UUID? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val birthDate: LocalDate? = null,
    val phoneNumber: String? = null,
    val termsAcceptanceAt: Instant? = null
) : Identifiable<UUID>
