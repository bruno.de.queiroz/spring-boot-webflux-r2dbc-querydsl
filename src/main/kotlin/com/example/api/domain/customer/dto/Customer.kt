package com.example.api.domain.customer.dto

import com.example.api.domain.Auditable
import com.example.api.domain.SoftDeletable
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import java.time.LocalDate
import java.util.UUID

@Table("customer")
data class Customer @PersistenceConstructor constructor(
    @Id
    @JvmField
    override var id: UUID?,

    @Column("created_at")
    @CreatedDate
    override val createdAt: Instant = Instant.now(),

    @Column("updated_at")
    @LastModifiedDate
    override val updatedAt: Instant = createdAt,

    @Column("deleted_at")
    override val deletedAt: Instant? = null,

    @Column("first_name")
    val firstName: String,

    @Column("last_name")
    val lastName: String,

    @Column("birth_date")
    val birthDate: LocalDate,

    @Column("phone_number")
    val phoneNumber: String,

    @Column("terms_acceptance_at")
    val termsAcceptanceAt: Instant = Instant.now()
) : SoftDeletable<UUID>, Auditable, Persistable<UUID> {
    // Needed because of this issue
    // https://github.com/spring-projects/spring-data-r2dbc/issues/49
    @Transient
    var newEntity: Boolean = true

    override fun isNew() = newEntity
    override fun getId() = id
}
