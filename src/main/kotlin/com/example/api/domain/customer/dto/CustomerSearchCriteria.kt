package com.example.api.domain.customer.dto

import org.springframework.web.bind.annotation.RequestParam
import java.util.UUID

data class CustomerSearchCriteria(
    @RequestParam(required = false) val text: String?,
    @RequestParam(required = false) val ids: List<UUID>?
)
