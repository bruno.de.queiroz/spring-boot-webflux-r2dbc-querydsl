package com.example.api.domain.customer.dto

import java.time.LocalDate
import java.time.chrono.ChronoPeriod
import java.time.temporal.ChronoUnit
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class CustomerInput(
    @field:NotEmpty
    @field:Size(min = 4, max = 100)
    val firstName: String?,

    @field:NotEmpty
    @field:Size(min = 4, max = 100)
    val lastName: String?,

    @field:NotNull
    val birthDate: LocalDate?,

    @field:NotEmpty
    @field:Size(min = 10, max = 20)
    @field:Pattern(regexp = "^\\+([0-9]{2})([0-9]+)$")
    val phoneNumber: String?
) {
    @AssertTrue
    fun isOver18YearsOld() = ChronoPeriod.between(birthDate, LocalDate.now()).get(ChronoUnit.YEARS) >= 18
}
