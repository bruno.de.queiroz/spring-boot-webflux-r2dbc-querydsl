package com.example.api.domain.customer

import com.example.api.domain.CrudService
import com.example.api.domain.customer.dto.Customer
import com.example.api.domain.customer.dto.CustomerTransient
import com.example.api.domain.customer.dto.QCustomer.customer
import com.example.api.extensions.contains
import com.example.api.extensions.toOrderSpecifier
import com.querydsl.core.BooleanBuilder
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import java.util.UUID
import javax.annotation.Nonnull

@Service
class CustomerService(override val mapper: CustomerMapper, override val repository: CustomerRepository) :
    CrudService<UUID, CustomerTransient, Customer>(mapper, repository) {

    fun findByCriteria(
        text: String?,
        ids: List<UUID>?,
        page: Pageable
    ): Flux<CustomerTransient> = repository
        .findAll(
            BooleanBuilder()
                .filterByNotDeleted()
                .filterByText(text)
                .filterByIds(ids),
            *page.sort.toOrderSpecifier()
        )
        .skip(page.offset)
        .take(page.pageSize.toLong())
        .map { mapper.asTransient(it) }

    override fun create(@Nonnull input: CustomerTransient): Mono<UUID> =
        repository.findById(input.id!!)
            .flatMap { Mono.error<UUID>(DataIntegrityViolationException("User has already a customer account")) }
            .switchIfEmpty {
                Mono.just(input)
                    .map { mapper.toModel(it) }
                    .flatMap { repository.save(it) }
                    .map { it.id!! }
            }

    private fun BooleanBuilder.filterByNotDeleted() = and(customer.deletedAt.isNull)

    private fun BooleanBuilder.filterByText(text: String?) = text?.takeIf { it.isNotBlank() }
        ?.let { and(customer.firstName.likeIgnoreCase("%$it%").or(customer.lastName.likeIgnoreCase("%$it%"))) }
        ?: this

    private fun BooleanBuilder.filterByIds(ids: List<UUID>?) = ids?.takeIf { it.isNotEmpty() }
        ?.let { and(customer.id.contains(it)) }
        ?: this
}
