package com.example.api.domain.customer

import com.example.api.domain.ClientMapper
import com.example.api.domain.ServiceMapper
import com.example.api.domain.customer.dto.Customer
import com.example.api.domain.customer.dto.CustomerInput
import com.example.api.domain.customer.dto.CustomerOutput
import com.example.api.domain.customer.dto.CustomerTransient
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class CustomerMapper :
    ClientMapper<CustomerInput, CustomerOutput, CustomerTransient>,
    ServiceMapper<CustomerTransient, Customer> {

    override fun toOutput(input: CustomerTransient) = CustomerOutput(
        id = input.id!!,
        firstName = input.firstName!!,
        lastName = input.lastName!!,
        birthDate = input.birthDate!!,
        phoneNumber = input.phoneNumber!!
    )

    override fun toTransient(input: CustomerInput) = CustomerTransient(
        firstName = input.firstName,
        lastName = input.lastName,
        birthDate = input.birthDate,
        phoneNumber = input.phoneNumber
    )

    override fun asTransient(input: Customer) = CustomerTransient(
        id = input.id,
        firstName = input.firstName,
        lastName = input.lastName,
        birthDate = input.birthDate,
        phoneNumber = input.phoneNumber,
        termsAcceptanceAt = input.termsAcceptanceAt
    )

    override fun toModel(input: CustomerTransient) = Customer(
        id = input.id!!,
        firstName = input.firstName!!,
        lastName = input.lastName!!,
        birthDate = input.birthDate!!,
        phoneNumber = input.phoneNumber!!
    )

    override fun merge(input: CustomerTransient, model: Customer) = Customer(
        id = model.id,
        firstName = input.firstName ?: model.firstName,
        lastName = input.lastName ?: model.lastName,
        birthDate = input.birthDate ?: model.birthDate,
        phoneNumber = input.phoneNumber ?: model.phoneNumber,
        termsAcceptanceAt = input.termsAcceptanceAt ?: model.termsAcceptanceAt,
        createdAt = model.createdAt,
        updatedAt = Instant.now()
    ).apply { newEntity = false }
}
