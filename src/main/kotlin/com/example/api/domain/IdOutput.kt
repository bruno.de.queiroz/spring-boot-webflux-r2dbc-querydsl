package com.example.api.domain

data class IdOutput<ID>(
    val id: ID?
)
