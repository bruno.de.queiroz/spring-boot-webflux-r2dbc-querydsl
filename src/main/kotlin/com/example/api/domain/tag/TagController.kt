package com.example.api.domain.tag

import com.example.api.domain.CrudController
import com.example.api.domain.tag.dto.Tag
import com.example.api.domain.tag.dto.TagInput
import com.example.api.domain.tag.dto.TagOutput
import com.example.api.domain.tag.dto.TagSearchCriteria
import com.example.api.domain.tag.dto.TagTransient
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/api/v1/tags", produces = [MediaType.APPLICATION_JSON_VALUE])
class TagController(override val mapper: TagMapper, override val service: TagService) :
    CrudController<UUID, TagInput, TagOutput, TagTransient, Tag>("tag", mapper, service) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun search(@Valid criteria: TagSearchCriteria, page: Pageable): Flux<TagOutput> =
        service.findByCriteria(
            text = criteria.text,
            ids = criteria.ids,
            page = page
        ).map { mapper.toOutput(it) }
}
