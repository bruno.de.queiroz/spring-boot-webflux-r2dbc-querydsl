package com.example.api.domain.tag

import com.example.api.domain.ClientMapper
import com.example.api.domain.ServiceMapper
import com.example.api.domain.tag.dto.Tag
import com.example.api.domain.tag.dto.TagInput
import com.example.api.domain.tag.dto.TagOutput
import com.example.api.domain.tag.dto.TagTransient
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class TagMapper :
    ClientMapper<TagInput, TagOutput, TagTransient>,
    ServiceMapper<TagTransient, Tag> {

    override fun toOutput(input: TagTransient) = TagOutput(
        id = input.id ?: throw IllegalStateException("tag id cannot be null"),
        slug = input.slug!!,
        name = input.name!!,
        description = input.description!!
    )

    override fun toTransient(input: TagInput) = TagTransient(
        id = null,
        slug = input.slug,
        name = input.name,
        description = input.description
    )

    override fun asTransient(input: Tag) = TagTransient(
        id = input.id,
        slug = input.slug,
        name = input.name,
        description = input.description
    )

    override fun toModel(input: TagTransient) = Tag(
        slug = input.slug!!,
        name = input.name!!,
        description = input.description!!
    )

    override fun merge(input: TagTransient, model: Tag) = Tag(
        id = model.id,
        name = input.name ?: model.name,
        slug = input.slug ?: model.slug,
        description = input.description ?: model.description,
        createdAt = model.createdAt,
        updatedAt = Instant.now()
    )
}
