package com.example.api.domain.tag.dto

import com.example.api.domain.Identifiable
import java.util.UUID

data class TagTransient(
    override val id: UUID?,
    val slug: String?,
    val name: String?,
    val description: String?
) : Identifiable<UUID>
