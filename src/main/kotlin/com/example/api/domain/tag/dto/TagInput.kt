package com.example.api.domain.tag.dto

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class TagInput(
    @field:NotEmpty
    @field:Size(min = 4, max = 50)
    val slug: String?,

    @field:NotEmpty
    @field:Size(min = 4, max = 100)
    val name: String?,

    @field:NotEmpty
    @field:Size(min = 4, max = 255)
    val description: String?
)
