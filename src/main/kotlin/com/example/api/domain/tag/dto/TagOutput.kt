package com.example.api.domain.tag.dto

import java.util.UUID

data class TagOutput(
    val id: UUID,
    val name: String,
    val slug: String,
    val description: String
)
