package com.example.api.domain.tag

import com.example.api.domain.CrudService
import com.example.api.domain.tag.dto.QTag.tag
import com.example.api.domain.tag.dto.Tag
import com.example.api.domain.tag.dto.TagTransient
import com.example.api.extensions.contains
import com.querydsl.core.BooleanBuilder
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.util.UUID

@Service
class TagService(override val mapper: TagMapper, override val repository: TagRepository) :
    CrudService<UUID, TagTransient, Tag>(mapper, repository) {

    fun findByCriteria(
        text: String?,
        ids: List<UUID>?,
        page: Pageable
    ): Flux<TagTransient> = repository
        .findAll(
            BooleanBuilder()
                .filterByNotDeleted()
                .filterByIds(ids)
                .filterByText(text),
            page.sort
        )
        .skip(page.offset)
        .take(page.pageSize.toLong())
        .map { mapper.asTransient(it) }

    private fun BooleanBuilder.filterByNotDeleted() = and(tag.deletedAt.isNull)

    private fun BooleanBuilder.filterByText(text: String?) = text?.takeIf { it.isNotBlank() }
        ?.let { and(tag.name.likeIgnoreCase("%$it%").or(tag.description.containsIgnoreCase(it))) }
        ?: this

    private fun BooleanBuilder.filterByIds(ids: List<UUID>?) = ids?.takeIf { it.isNotEmpty() }
        ?.let { and(tag.id.contains(it)) }
        ?: this
}
