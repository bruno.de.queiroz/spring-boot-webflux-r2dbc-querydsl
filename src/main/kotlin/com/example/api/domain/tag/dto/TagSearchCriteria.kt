package com.example.api.domain.tag.dto

import org.springframework.web.bind.annotation.RequestParam
import java.util.UUID

data class TagSearchCriteria(
    @RequestParam(required = false) val text: String?,
    @RequestParam(required = false) val ids: List<UUID>?
)
