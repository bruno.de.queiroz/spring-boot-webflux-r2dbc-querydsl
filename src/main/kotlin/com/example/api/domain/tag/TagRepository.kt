package com.example.api.domain.tag

import com.example.api.domain.CrudRepository
import com.example.api.domain.tag.dto.Tag
import org.springframework.validation.annotation.Validated
import java.util.UUID

@Validated
interface TagRepository : CrudRepository<Tag, UUID>
