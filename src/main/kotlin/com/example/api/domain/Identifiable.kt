package com.example.api.domain

import javax.annotation.Nullable

interface Identifiable<ID> {
    @Nullable
    val id: ID?
}
