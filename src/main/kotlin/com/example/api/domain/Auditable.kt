package com.example.api.domain

import java.time.Instant

interface Auditable {
    val createdAt: Instant
    val updatedAt: Instant
}
