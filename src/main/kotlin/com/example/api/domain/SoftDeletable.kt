package com.example.api.domain

import java.time.Instant

interface SoftDeletable<ID> : Identifiable<ID> {
    val deletedAt: Instant?
}
