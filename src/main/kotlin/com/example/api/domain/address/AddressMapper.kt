package com.example.api.domain.address

import com.example.api.domain.address.dto.Address
import com.example.api.domain.address.dto.AddressInput
import com.example.api.domain.address.dto.AddressOutput
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class AddressMapper {

    fun toOutput(address: Address): AddressOutput =
        AddressOutput(
            id = address.id ?: throw Error("Address id cannot be null"),
            firstName = address.firstName,
            lastName = address.lastName,
            phoneNumber = address.phoneNumber,
            line1 = address.line1,
            line2 = address.line2,
            companyName = address.companyName,
            postalCode = address.postalCode,
            latitude = address.latitude,
            longitude = address.longitude,
            district = address.district,
            city = address.city,
            country = address.country,
            createdAt = address.createdAt,
            updatedAt = address.updatedAt
        )

    fun toModel(userId: UUID, address: AddressInput): Address = Address(
        userId = userId,
        firstName = address.firstName,
        lastName = address.lastName,
        phoneNumber = address.phoneNumber,
        line1 = address.line1!!,
        line2 = address.line2,
        companyName = address.companyName,
        postalCode = address.postalCode!!,
        latitude = address.latitude!!,
        longitude = address.longitude!!,
        district = address.district!!,
        city = address.city!!,
        country = address.country!!
    )
}
