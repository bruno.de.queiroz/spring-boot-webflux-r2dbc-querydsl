package com.example.api.domain.address

import com.example.api.domain.address.dto.AddressInput
import com.example.api.domain.address.dto.AddressOutput
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.UUID

@Service
class AddressService(
    private val addressRepository: AddressRepository,
    private val addressMapper: AddressMapper
) {
    fun findById(id: UUID): Mono<AddressOutput> =
        addressRepository.findById(id)
            .map { addressMapper.toOutput(it) }

    fun findAll(customerId: UUID): Mono<List<AddressOutput>> =
        addressRepository.findByUserId(customerId)
            .map { addressMapper.toOutput(it) }
            .collectList()

    fun create(userId: UUID, input: AddressInput): Mono<AddressOutput> =
        addressRepository.save(addressMapper.toModel(userId, input))
            .map { addressMapper.toOutput(it) }
}
