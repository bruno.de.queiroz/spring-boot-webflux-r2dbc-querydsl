package com.example.api.domain.address.dto

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import java.util.UUID

@Table
data class Address @PersistenceConstructor constructor(
    @Id
    val id: UUID? = null,

    @Column("user_id")
    val userId: UUID,

    @Column("first_name")
    val firstName: String? = null,

    @Column("last_name")
    val lastName: String? = null,

    @Column("phone_number")
    val phoneNumber: String? = null,

    @Column("alias")
    val alias: String = "Home",

    @Column("line_1")
    val line1: String,

    @Column("line_2")
    val line2: String? = null,

    @Column("company_name")
    val companyName: String? = null,

    @Column("postal_code")
    val postalCode: String,

    @Column("latitude")
    val latitude: Double,

    @Column("longitude")
    val longitude: Double,

    @Column("district")
    val district: String,

    @Column("city")
    val city: String,

    @Column("country")
    val country: String,

    @Column("created_at")
    @CreatedDate
    val createdAt: Instant = Instant.now(),

    @Column("updated_at")
    @LastModifiedDate
    val updatedAt: Instant = createdAt,

    @Column("deleted_at")
    val deletedAt: Instant? = null
)
