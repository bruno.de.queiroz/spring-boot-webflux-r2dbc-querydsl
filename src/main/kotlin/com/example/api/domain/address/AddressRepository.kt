package com.example.api.domain.address

import com.example.api.domain.address.dto.Address
import com.infobip.spring.data.r2dbc.QuerydslR2dbcRepository
import org.springframework.validation.annotation.Validated
import reactor.core.publisher.Flux
import java.util.UUID

@Validated
interface AddressRepository : QuerydslR2dbcRepository<Address, UUID> {

    fun findByUserId(userId: UUID): Flux<Address>
}
