package com.example.api.domain.address.dto

import java.time.Instant
import java.util.UUID

data class AddressOutput(
    val id: UUID,
    val firstName: String?,
    val lastName: String?,
    val phoneNumber: String?,
    val line1: String,
    val line2: String?,
    val companyName: String?,
    val postalCode: String,
    val latitude: Double,
    val longitude: Double,
    val district: String,
    val city: String,
    val country: String,
    val createdAt: Instant,
    val updatedAt: Instant
)
