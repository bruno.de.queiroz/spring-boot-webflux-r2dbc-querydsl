package com.example.api.domain.address.dto

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class AddressInput(
    @field:NotEmpty
    @field:Size(min = 4, max = 20)
    val alias: String?,

    @field:Size(min = 3, max = 20)
    val firstName: String? = null,

    @field:Size(min = 3, max = 20)
    val lastName: String? = null,

    @field:Size(min = 10, max = 20)
    @field:Pattern(regexp = "^\\+([0-9]{2})([0-9]+)$")
    val phoneNumber: String? = null,

    @field:NotEmpty
    @field:Size(min = 5, max = 255)
    val line1: String?,

    @field:Size(min = 5, max = 255)
    val line2: String? = null,

    @field:Size(min = 5, max = 255)
    val companyName: String? = null,

    @field:NotEmpty
    @field:Size(min = 3, max = 20)
    val postalCode: String?,

    @field:NotNull
    val latitude: Double?,

    @field:NotNull
    val longitude: Double?,

    @field:NotEmpty
    @field:Size(min = 3, max = 100)
    val district: String?,

    @field:NotEmpty
    @field:Size(min = 3, max = 100)
    val city: String?,

    @field:NotEmpty
    @field:Size(min = 3, max = 100)
    val country: String?
)
