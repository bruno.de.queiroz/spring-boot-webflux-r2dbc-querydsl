package com.example.api.domain.address

import com.example.api.domain.IdOutput
import com.example.api.domain.address.dto.AddressInput
import com.example.api.domain.address.dto.AddressOutput
import com.example.api.extensions.notFoundIfEmpty
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/api/v1/users/{userId}/addresses", produces = [MediaType.APPLICATION_JSON_VALUE])
class AddressController(private val service: AddressService) {

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #userId.toString()) or hasAuthority('ADMIN')")
    fun fetch(@PathVariable userId: UUID, @PathVariable id: UUID): Mono<AddressOutput> =
        service.findById(id).notFoundIfEmpty("Address not found")

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #userId.toString()) or hasAuthority('ADMIN')")
    fun fetchAll(@PathVariable userId: UUID): Mono<List<AddressOutput>> =
        service.findAll(userId).notFoundIfEmpty("Address not found")

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #userId.toString()) or hasAuthority('ADMIN')")
    fun create(@PathVariable userId: UUID, @RequestBody @Valid address: AddressInput): Mono<IdOutput<UUID>> =
        service.create(userId, address).map { IdOutput(it.id) }
}
