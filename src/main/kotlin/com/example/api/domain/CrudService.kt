package com.example.api.domain

import reactor.core.publisher.Mono
import javax.annotation.Nonnull

abstract class CrudService<ID, DTO : Identifiable<ID>, MODEL : SoftDeletable<ID>>(
    protected open val mapper: ServiceMapper<DTO, MODEL>,
    protected open val repository: CrudRepository<MODEL, ID>
) {

    fun findById(id: ID): Mono<DTO> =
        repository.findActive(id).map { mapper.asTransient(it) }

    open fun create(@Nonnull input: DTO): Mono<ID> =
        Mono.just(input)
            .map { mapper.toModel(it) }
            .flatMap { repository.save(it) }
            .map { it.id!! }

    open fun update(id: ID, input: DTO): Mono<ID> =
        repository.findActive(id)
            .map { mapper.merge(input, it) }
            .flatMap { repository.save(it) }
            .map { it.id!! }

    fun delete(input: DTO): Mono<Void> =
        Mono.just(input)
            .flatMap { repository.deactivate(it.id!!) }
            .then()
}
