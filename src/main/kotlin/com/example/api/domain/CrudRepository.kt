package com.example.api.domain

import com.example.querydsl.SoftDeleteFragment
import com.infobip.spring.data.r2dbc.QuerydslR2dbcRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface CrudRepository<MODEL : SoftDeletable<ID>, ID> : QuerydslR2dbcRepository<MODEL, ID>, SoftDeleteFragment<MODEL, ID>
