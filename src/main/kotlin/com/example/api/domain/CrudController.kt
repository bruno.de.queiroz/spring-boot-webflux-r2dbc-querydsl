package com.example.api.domain

import com.example.api.exception.ErrorResponse
import com.example.api.extensions.notFoundIfEmpty
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import reactor.core.publisher.Mono
import javax.validation.Valid

abstract class CrudController<ID, IN : Any, OUT : Any, TRANSIENT : Identifiable<ID>, MODEL : SoftDeletable<ID>>(
    private val name: String,
    protected open val mapper: ClientMapper<IN, OUT, TRANSIENT>,
    protected open val service: CrudService<ID, TRANSIENT, MODEL>
) {

    @Operation(summary = "Create resource", description = "Returns the id of the resource created")
    @ApiResponses(
        ApiResponse(responseCode = "202", description = "Created with success"),
        ApiResponse(responseCode = "400", description = "Bad body parameters", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "409", description = "Conflict creating the resource", content = [Content(schema = Schema(implementation = ErrorResponse::class))])
    )
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('ADMIN')")
    open fun create(@RequestBody @Valid input: IN): Mono<IdOutput<ID>> =
        Mono.just(input)
            .map { mapper.toTransient(it) }
            .flatMap { service.create(it) }
            .map { IdOutput(it) }

    @Operation(summary = "Fetch resource by id", description = "Returns the resource based on the id")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Resource found with success"),
        ApiResponse(responseCode = "404", description = "Resource not found")
    )
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    open fun fetch(@PathVariable id: ID): Mono<OUT> =
        service.findById(id)
            .map { mapper.toOutput(it) }
            .notFoundIfEmpty("$name not found")

    @Operation(summary = "Update a resource by id", description = "Returns the resource id")
    @ApiResponses(
        ApiResponse(responseCode = "202", description = "Resource updated with success"),
        ApiResponse(responseCode = "400", description = "Bad body parameters", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", description = "Resource not found"),
        ApiResponse(responseCode = "409", description = "Resource could not be update due conflict", content = [Content(schema = Schema(implementation = ErrorResponse::class))])
    )
    @PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("hasAuthority('ADMIN')")
    open fun update(@PathVariable id: ID, @RequestBody @Valid input: IN): Mono<IdOutput<ID>> =
        Mono.just(input)
            .map { mapper.toTransient(it) }
            .flatMap { service.update(id, it) }
            .map { IdOutput(it) }
            .notFoundIfEmpty("$name not found")

    @Operation(summary = "Delete a resource by id", description = "Returns the no content")
    @ApiResponses(
        ApiResponse(responseCode = "204", description = "Resource deleted with success"),
        ApiResponse(responseCode = "404", description = "Resource not found")
    )
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ADMIN')")
    open fun delete(@PathVariable id: ID): Mono<Void> =
        service.findById(id)
            .notFoundIfEmpty("$name not found")
            .flatMap { service.delete(it) }
}
