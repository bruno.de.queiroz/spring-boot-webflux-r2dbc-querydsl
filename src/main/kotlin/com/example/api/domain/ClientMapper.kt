package com.example.api.domain

interface ClientMapper<I : Any, O : Any, D : Any> {
    fun toOutput(input: D): O
    fun toTransient(input: I): D
}
