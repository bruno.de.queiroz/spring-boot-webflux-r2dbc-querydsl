package com.example.api.domain

interface ServiceMapper<D : Any, M : Any> {
    fun asTransient(input: M): D
    fun toModel(input: D): M
    fun merge(input: D, model: M): M
}
