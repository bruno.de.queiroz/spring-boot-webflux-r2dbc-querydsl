package com.example.api.domain.user

import com.example.api.domain.IdOutput
import com.example.api.domain.user.dto.UserInput
import com.example.api.domain.user.dto.UserOutput
import com.example.api.extensions.notFoundIfEmpty
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/api/v1/users", produces = [MediaType.APPLICATION_JSON_VALUE])
internal class UserController(private val service: UserService) {

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("(hasAuthority('CUSTOMER') and principal.subject == #id.toString()) or hasAuthority('ADMIN')")
    fun fetch(@PathVariable id: UUID): Mono<UserOutput> =
        service.findById(id).notFoundIfEmpty("Customer not found")

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody @Valid user: UserInput): Mono<IdOutput<UUID>> =
        service.create(user).map { IdOutput(it.id) }
}
