package com.example.api.domain.user.auth

import com.example.api.domain.user.UserMapper
import com.example.api.domain.user.UserRepository
import com.github.scribejava.core.oauth.OAuth20Service
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

@Service
class GoogleAuthenticationService(
    @Qualifier("googleOAuthService") googleOAuthService: OAuth20Service,
    userRepository: UserRepository,
    userMapper: UserMapper,
    passwordGenerator: PasswordGenerator,
    tokenGenerationService: TokenGenerationService
) : TokenAuthenticationService(
    googleOAuthService,
    "https://www.googleapis.com/oauth2/v3/userinfo",
    userRepository,
    userMapper,
    passwordGenerator,
    tokenGenerationService
)
