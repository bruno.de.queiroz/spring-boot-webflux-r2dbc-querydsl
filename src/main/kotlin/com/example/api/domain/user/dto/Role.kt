package com.example.api.domain.user.dto

enum class Role {
    CUSTOMER, ADMIN
}
