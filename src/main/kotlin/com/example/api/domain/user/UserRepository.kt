package com.example.api.domain.user

import com.example.api.domain.user.dto.User
import com.infobip.spring.data.r2dbc.QuerydslR2dbcRepository
import org.springframework.validation.annotation.Validated
import reactor.core.publisher.Mono
import java.util.UUID

@Validated
interface UserRepository : QuerydslR2dbcRepository<User, UUID> {
    fun findByEmail(email: String): Mono<User>
}
