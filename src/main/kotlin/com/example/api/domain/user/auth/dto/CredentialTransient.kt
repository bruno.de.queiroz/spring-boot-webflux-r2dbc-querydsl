package com.example.api.domain.user.auth.dto

data class CredentialTransient(
    val email: String? = null,
    val password: String? = null
) : AuthTransient
