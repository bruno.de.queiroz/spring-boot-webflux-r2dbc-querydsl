package com.example.api.domain.user.auth

import com.example.api.domain.user.UserRepository
import com.example.api.domain.user.auth.dto.CredentialTransient
import com.example.api.domain.user.auth.dto.TokenTransient
import com.example.api.domain.user.auth.exception.AuthenticationError
import com.example.api.domain.user.auth.exception.AuthenticationException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty

@Service
class CredentialAuthenticationService(
    private val userRepository: UserRepository,
    private val tokenGenerationService: TokenGenerationService,
    private val passwordEncoder: PasswordEncoder
) : AuthenticationService<CredentialTransient> {

    override fun authenticate(input: CredentialTransient): Mono<TokenTransient> =
        Mono.justOrEmpty(input.email)
            .switchIfEmpty { Mono.error(AuthenticationException(AuthenticationError.NO_EMAIL_PROVIDED)) }
            .flatMap { email -> Mono.justOrEmpty(input.password).map { email to it } }
            .switchIfEmpty { Mono.error(AuthenticationException(AuthenticationError.NO_PASSWORD_PROVIDED)) }
            .flatMap { cred -> userRepository.findByEmail(cred.first).filter { isPasswordValid(cred.second, it.password) } }
            .switchIfEmpty { Mono.error(AuthenticationException(AuthenticationError.NOT_FOUND)) }
            .filter { it.deletedAt == null }
            .switchIfEmpty { Mono.error(AuthenticationException(AuthenticationError.NOT_FOUND)) }
            .map { user ->
                TokenTransient(
                    token = tokenGenerationService.generateToken(
                        id = user.id!!,
                        roles = user.roles.map { it.name }
                    ),
                    email = user.email,
                    completed = true,
                    verified = user.verifiedAt != null
                )
            }

    private fun isPasswordValid(inputPass: String, userPass: String) = passwordEncoder.matches(inputPass, userPass)
}
