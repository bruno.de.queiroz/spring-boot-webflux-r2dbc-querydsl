package com.example.api.domain.user.auth

enum class AuthenticationProvider {
    CREDENTIAL,
    FACEBOOK,
    GOOGLE
}
