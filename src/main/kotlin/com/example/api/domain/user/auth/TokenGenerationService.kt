package com.example.api.domain.user.auth

import com.example.api.ApplicationProperties
import com.example.api.domain.user.dto.Role
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.Date
import java.util.UUID

@Component
class TokenGenerationService(private val jwtProperties: ApplicationProperties.JwtProperties) {

    private val key = Keys.hmacShaKeyFor(jwtProperties.secret.toByteArray())

    fun validateToken(token: String): Boolean = !isTokenExpired(token)

    fun generateToken(id: UUID, roles: Collection<String>): String =
        doGenerateToken(mapOf("roles" to roles), id)

    fun getAuthoritiesFromClaims(claims: Claims): List<GrantedAuthority> =
        claims.get("roles", List::class.java)
            .filterIsInstance(String::class.java)
            .mapNotNull { kotlin.runCatching { Role.valueOf(it) }.getOrNull() }
            .map { SimpleGrantedAuthority(it.name) }

    fun parseJwtToken(token: String): Jws<Claims>? = kotlin.runCatching { Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token) }.getOrNull()

    private fun isTokenExpired(token: String): Boolean =
        parseJwtToken(token)?.body?.expiration?.toInstant()?.isBefore(Instant.ofEpochSecond(jwtProperties.expiration))
            ?: true

    private fun doGenerateToken(claims: Map<String, Any>, id: UUID): String {
        val createdDate = Instant.now()
        val expirationDate = createdDate.plusSeconds(jwtProperties.expiration)
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(id.toString())
            .setIssuedAt(Date.from(createdDate))
            .setExpiration(Date.from(expirationDate))
            .signWith(key)
            .compact()
    }
}
