package com.example.api.domain.user.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class UserInput(
    @field:NotEmpty
    @field:Size(min = 6, max = 100)
    @field:Email
    val email: String?,

    @field:NotEmpty
    @field:Size(min = 6, max = 100)
    val password: String?
)
