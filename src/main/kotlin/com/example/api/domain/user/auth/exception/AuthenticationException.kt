package com.example.api.domain.user.auth.exception

data class AuthenticationException(val code: AuthenticationError, val ex: Throwable? = null) : Exception()
