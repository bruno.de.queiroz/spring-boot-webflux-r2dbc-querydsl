package com.example.api.domain.user.auth.dto

data class TokenTransient(
    val token: String? = null,
    val email: String? = null,
    val completed: Boolean? = null,
    val verified: Boolean? = null
) : AuthTransient
