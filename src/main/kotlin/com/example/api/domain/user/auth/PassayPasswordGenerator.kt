package com.example.api.domain.user.auth

import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.springframework.stereotype.Component

@Component
internal class PassayPasswordGenerator : PasswordGenerator {

    companion object {
        private var rules = listOf(
            CharacterRule(EnglishCharacterData.UpperCase, 2),
            CharacterRule(EnglishCharacterData.LowerCase, 2),
            CharacterRule(EnglishCharacterData.Digit, 2),
            CharacterRule(EnglishCharacterData.Special, 2)
        )

        private var passwordGenerator = org.passay.PasswordGenerator()
    }

    override fun generatePassword(length: Int): String = passwordGenerator.generatePassword(length, rules)
}
