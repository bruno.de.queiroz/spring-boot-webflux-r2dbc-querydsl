package com.example.api.domain.user.dto

enum class PasswordHash {
    BCRYPT
}
