package com.example.api.domain.user.dto

import java.time.Instant
import java.util.UUID

data class UserOutput(
    val id: UUID,
    val email: String,
    val createdAt: Instant,
    val updatedAt: Instant
)
