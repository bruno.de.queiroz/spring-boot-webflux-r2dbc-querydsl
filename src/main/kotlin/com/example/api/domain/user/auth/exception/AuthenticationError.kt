package com.example.api.domain.user.auth.exception

enum class AuthenticationError {
    NOT_FOUND,
    UNKNOWN_PROVIDER,
    NO_EMAIL_PROVIDED,
    NO_PASSWORD_PROVIDED,
    NO_TOKEN_PROVIDED,
    WRONG_TOKEN,
    FETCHING_USER_PROFILE
}
