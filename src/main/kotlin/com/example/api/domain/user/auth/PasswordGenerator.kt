package com.example.api.domain.user.auth

interface PasswordGenerator {

    fun generatePassword(length: Int): String
}
