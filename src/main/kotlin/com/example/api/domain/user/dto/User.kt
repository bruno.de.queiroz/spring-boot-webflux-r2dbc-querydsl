package com.example.api.domain.user.dto

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import java.util.UUID

@Table("auth")
data class User @PersistenceConstructor constructor(
    @Id
    val id: UUID? = null,

    @Column("email")
    val email: String,

    @Column("password")
    val password: String,

    @Column("password_hash")
    val passwordHash: PasswordHash,

    @Column("roles")
    val roles: Set<Role>,

    @Column("created_at")
    @CreatedDate
    val createdAt: Instant = Instant.now(),

    @Column("updated_at")
    @LastModifiedDate
    val updatedAt: Instant = createdAt,

    @Column("deleted_at")
    val deletedAt: Instant? = null,

    @Column("verified_at")
    val verifiedAt: Instant? = null
)
