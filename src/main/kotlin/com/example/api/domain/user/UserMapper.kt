package com.example.api.domain.user

import com.example.api.domain.user.dto.PasswordHash
import com.example.api.domain.user.dto.Role
import com.example.api.domain.user.dto.User
import com.example.api.domain.user.dto.UserInput
import com.example.api.domain.user.dto.UserOutput
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserMapper(private val passwordEncoder: PasswordEncoder) {

    fun toOutput(user: User) = UserOutput(
        id = user.id ?: throw IllegalStateException("User id cannot be null"),
        email = user.email,
        createdAt = user.createdAt,
        updatedAt = user.updatedAt
    )

    fun toModel(user: UserInput): User = User(
        email = user.email!!,
        password = passwordEncoder.encode(user.password),
        passwordHash = PasswordHash.BCRYPT,
        roles = setOf(Role.CUSTOMER)
    )
}
