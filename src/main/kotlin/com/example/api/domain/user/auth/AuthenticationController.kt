package com.example.api.domain.user.auth

import com.example.api.domain.user.auth.dto.CredentialInput
import com.example.api.domain.user.auth.dto.CredentialTransient
import com.example.api.domain.user.auth.dto.TokenInput
import com.example.api.domain.user.auth.dto.TokenOutput
import com.example.api.domain.user.auth.dto.TokenTransient
import com.example.api.domain.user.auth.exception.AuthenticationError
import com.example.api.domain.user.auth.exception.AuthenticationException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException
import reactor.core.publisher.Mono
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/auth", produces = [MediaType.APPLICATION_JSON_VALUE])
internal class AuthenticationController(private val facade: AuthenticationFacade) {

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun withCredential(@RequestBody @Valid input: CredentialInput): Mono<TokenOutput> =
        facade.authenticate(AuthenticationProvider.CREDENTIAL, CredentialTransient(input.email!!, input.password!!))
            .toOutput()
            .onErrorResume { Mono.error(handleAuthException(it)) }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/facebook")
    fun withFacebook(@RequestBody @Valid input: TokenInput): Mono<TokenOutput> =
        facade.authenticate(AuthenticationProvider.FACEBOOK, TokenTransient(input.token!!))
            .toOutput()
            .onErrorResume { Mono.error(handleAuthException(it)) }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/google")
    fun withGoogle(@RequestBody @Valid input: TokenInput): Mono<TokenOutput> =
        facade.authenticate(AuthenticationProvider.GOOGLE, TokenTransient(input.token!!))
            .toOutput()
            .onErrorResume { Mono.error(handleAuthException(it)) }

    fun handleAuthException(exception: Throwable) =
        if (exception is AuthenticationException) {
            when (exception.code) {
                AuthenticationError.NOT_FOUND -> HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Credentials not found")
                AuthenticationError.UNKNOWN_PROVIDER -> HttpClientErrorException(HttpStatus.BAD_REQUEST, "Provider not supported")
                AuthenticationError.NO_EMAIL_PROVIDED -> HttpClientErrorException(HttpStatus.BAD_REQUEST, "Email not provided")
                AuthenticationError.NO_PASSWORD_PROVIDED -> HttpClientErrorException(HttpStatus.BAD_REQUEST, "Password not provided")
                AuthenticationError.NO_TOKEN_PROVIDED -> HttpClientErrorException(HttpStatus.BAD_REQUEST, "Token not provided")
                AuthenticationError.WRONG_TOKEN -> HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Token provided is wrong")
                AuthenticationError.FETCHING_USER_PROFILE -> HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Error fetching user profile")
            }
        } else {
            exception
        }

    private fun Mono<TokenTransient>.toOutput() =
        this.map {
            TokenOutput(
                token = it.token!!,
                email = it.email!!,
                completed = it.completed!!,
                verified = it.verified!!
            )
        }
}
