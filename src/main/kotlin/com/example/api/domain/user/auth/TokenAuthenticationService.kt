package com.example.api.domain.user.auth

import com.example.api.domain.user.UserMapper
import com.example.api.domain.user.UserRepository
import com.example.api.domain.user.auth.dto.TokenTransient
import com.example.api.domain.user.auth.exception.AuthenticationError
import com.example.api.domain.user.auth.exception.AuthenticationException
import com.example.api.domain.user.dto.User
import com.example.api.domain.user.dto.UserInput
import com.example.api.extensions.toMono
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import com.github.scribejava.core.oauth.OAuth20Service
import org.json.JSONObject
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty

abstract class TokenAuthenticationService(
    private val oAuth20Service: OAuth20Service,
    private val profileURL: String,
    private val userRepository: UserRepository,
    private val userMapper: UserMapper,
    private val passwordGenerator: PasswordGenerator,
    private val tokenGenerationService: TokenGenerationService
) : AuthenticationService<TokenTransient> {

    companion object {
        private const val DEFAULT_TIMEOUT_IN_SECONDS = 3L
        private val log = LoggerFactory.getLogger(this::class.java)
    }

    override fun authenticate(input: TokenTransient): Mono<TokenTransient> =
        Mono.justOrEmpty(input.token)
            .switchIfEmpty { Mono.error(AuthenticationException(AuthenticationError.NO_TOKEN_PROVIDED)) }
            .flatMap { getEmail(it) }
            .flatMap { email ->
                userRepository.findByEmail(email)
                    .toAuthenticationDTO(true)
                    .switchIfEmpty {
                        userRepository.save(userWithoutPassword(email))
                            .toAuthenticationDTO(false)
                    }
            }

    private fun Mono<User>.toAuthenticationDTO(existing: Boolean) =
        this.map { user ->
            TokenTransient(
                token = tokenGenerationService.generateToken(
                    id = user.id!!,
                    roles = user.roles.map { it.name }
                ),
                email = user.email,
                completed = existing,
                verified = user.verifiedAt != null
            )
        }

    private fun getEmail(token: String): Mono<String> =
        Mono.just(OAuthRequest(Verb.GET, profileURL))
            .doOnNext { oAuth20Service.signRequest(token, it) }
            .flatMap {
                oAuth20Service.executeAsync(it)
                    .toMono(timeout = DEFAULT_TIMEOUT_IN_SECONDS)
            }
            .map { JSONObject(it.body) }
            .map { it.getString("email") }
            .onErrorResume {
                log.error("{}", it)
                Mono.error(AuthenticationException(AuthenticationError.FETCHING_USER_PROFILE, it))
            }

    private fun userWithoutPassword(email: String) =
        userMapper.toModel(UserInput(email, passwordGenerator.generatePassword(22)))
}
