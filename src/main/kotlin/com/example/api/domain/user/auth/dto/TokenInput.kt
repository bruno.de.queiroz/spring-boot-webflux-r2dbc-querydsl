package com.example.api.domain.user.auth.dto

import javax.validation.constraints.NotEmpty

data class TokenInput(
    @field:NotEmpty
    val token: String? = null
)
