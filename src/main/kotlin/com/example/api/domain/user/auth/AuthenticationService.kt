package com.example.api.domain.user.auth

import com.example.api.domain.user.auth.dto.AuthTransient
import com.example.api.domain.user.auth.dto.TokenTransient
import reactor.core.publisher.Mono

interface AuthenticationService<T : AuthTransient> {

    fun authenticate(input: T): Mono<TokenTransient>
}
