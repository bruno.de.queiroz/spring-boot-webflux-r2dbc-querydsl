package com.example.api.domain.user.auth.dto

data class TokenOutput(
    val token: String,
    val email: String,
    val completed: Boolean,
    val verified: Boolean
)
