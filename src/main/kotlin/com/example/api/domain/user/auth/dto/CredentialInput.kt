package com.example.api.domain.user.auth.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class CredentialInput(
    @field:NotEmpty
    @field:Size(min = 6, max = 100)
    @field:Email
    val email: String? = null,

    @field:NotEmpty
    @field:Size(min = 6, max = 255)
    val password: String? = null
)
