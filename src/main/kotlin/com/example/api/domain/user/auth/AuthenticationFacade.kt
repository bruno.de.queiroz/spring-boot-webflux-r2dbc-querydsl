package com.example.api.domain.user.auth

import com.example.api.domain.user.auth.dto.CredentialTransient
import com.example.api.domain.user.auth.dto.TokenTransient
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
internal class AuthenticationFacade(
    private val credentialService: CredentialAuthenticationService,
    private val facebookService: FacebookAuthenticationService,
    private val googleService: GoogleAuthenticationService
) {
    fun <T> authenticate(provider: AuthenticationProvider, input: T): Mono<TokenTransient> =
        when (provider) {
            AuthenticationProvider.CREDENTIAL -> credentialService.authenticate(input as CredentialTransient)
            AuthenticationProvider.FACEBOOK -> facebookService.authenticate(input as TokenTransient)
            AuthenticationProvider.GOOGLE -> googleService.authenticate(input as TokenTransient)
        }
}
