package com.example.api.domain.user.auth

import com.example.api.domain.user.UserMapper
import com.example.api.domain.user.UserRepository
import com.github.scribejava.core.oauth.OAuth20Service
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

@Service
class FacebookAuthenticationService(
    @Qualifier("facebookOAuthService") facebookOAuthService: OAuth20Service,
    userRepository: UserRepository,
    userMapper: UserMapper,
    passwordGenerator: PasswordGenerator,
    tokenGenerationService: TokenGenerationService
) : TokenAuthenticationService(
    facebookOAuthService,
    "https://graph.facebook.com/v3.2/me?fields=email",
    userRepository,
    userMapper,
    passwordGenerator,
    tokenGenerationService
)
