package com.example.api.domain.user

import com.example.api.domain.user.dto.UserInput
import com.example.api.domain.user.dto.UserOutput
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.UUID

@Service
class UserService(
    private val userRepository: UserRepository,
    private val userMapper: UserMapper
) {

    fun findById(id: UUID): Mono<UserOutput> =
        userRepository.findById(id)
            .map { userMapper.toOutput(it) }

    fun create(input: UserInput): Mono<UserOutput> =
        userRepository.save(userMapper.toModel(input))
            .map { userMapper.toOutput(it) }
}
