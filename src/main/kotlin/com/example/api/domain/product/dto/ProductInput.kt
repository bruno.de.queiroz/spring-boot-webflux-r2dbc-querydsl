package com.example.api.domain.product.dto

import java.math.BigDecimal
import java.util.UUID
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class ProductInput(
    @field:NotEmpty
    @field:Size(min = 4, max = 100)
    val name: String?,

    @field:NotEmpty
    @field:Size(min = 4, max = 100)
    val slug: String?,

    @field:NotEmpty
    @field:Size(min = 4, max = 255)
    val description: String?,

    @field:NotNull
    @field:DecimalMin(value = "0.01")
    val price: BigDecimal?,

    @field:NotNull
    @field:Min(value = 1)
    val value: Long?,

    @field:NotNull
    val unit: UnitType?,

    @field:NotNull
    @field:Min(value = 1)
    val quantity: Long?,

    val tags: List<UUID>?
)
