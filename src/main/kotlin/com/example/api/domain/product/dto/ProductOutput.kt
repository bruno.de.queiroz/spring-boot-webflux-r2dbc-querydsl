package com.example.api.domain.product.dto

import java.math.BigDecimal
import java.util.UUID

open class ProductOutput(
    open val id: UUID,
    open val slug: String?,
    open val name: String?,
    open val description: String?,
    open val price: BigDecimal?,
    open val value: Long?,
    open val unit: UnitType?,
    open val available: Boolean?,
    open val tags: List<UUID>?
)
