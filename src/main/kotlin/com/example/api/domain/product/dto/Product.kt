package com.example.api.domain.product.dto

import com.example.api.domain.Auditable
import com.example.api.domain.SoftDeletable
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

@Table("product")
data class Product @PersistenceConstructor constructor(
    @Id
    override val id: UUID? = null,

    @Column("created_at")
    @CreatedDate
    override val createdAt: Instant = Instant.now(),

    @Column("updated_at")
    @LastModifiedDate
    override val updatedAt: Instant = createdAt,

    @Column("deleted_at")
    override val deletedAt: Instant? = null,

    @Column("slug")
    val slug: String,

    @Column("name")
    val name: String,

    @Column("description")
    val description: String,

    @Column("price")
    val price: BigDecimal,

    @Column("value")
    val value: Long,

    @Column("unit")
    val unit: UnitType,

    @Column("quantity")
    val quantity: Long,

    @Column("tags")
    val tags: List<UUID>? = emptyList()
) : SoftDeletable<UUID>, Auditable
