package com.example.api.domain.product

import com.example.api.domain.CrudService
import com.example.api.domain.product.dto.Product
import com.example.api.domain.product.dto.ProductTransient
import com.example.api.domain.product.dto.QProduct.product
import com.example.api.extensions.contains
import com.querydsl.core.BooleanBuilder
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.math.BigDecimal
import java.util.UUID

@Service
class ProductService(override val mapper: ProductMapper, override val repository: ProductRepository) :
    CrudService<UUID, ProductTransient, Product>(mapper, repository) {

    fun findByCriteria(
        text: String?,
        ids: List<UUID>?,
        tags: List<UUID>?,
        minPrice: BigDecimal?,
        maxPrice: BigDecimal?,
        minValue: Long?,
        maxValue: Long?,
        page: Pageable
    ): Flux<ProductTransient> = repository
        .findAll(
            BooleanBuilder()
                .filterByNotDeleted()
                .filterByText(text)
                .filterByIds(ids)
                .filterByTags(tags)
                .filterByPriceRange(minPrice, maxPrice)
                .filterByValueRange(minValue, maxValue),
            page.sort
        )
        .skip(page.offset)
        .take(page.pageSize.toLong())
        .map { mapper.asTransient(it) }

    private fun BooleanBuilder.filterByNotDeleted() = and(product.deletedAt.isNull)

    private fun BooleanBuilder.filterByText(text: String?) = text?.takeIf { it.isNotBlank() }
        ?.let { and(product.name.likeIgnoreCase("%$it%").or(product.description.containsIgnoreCase(it))) }
        ?: this

    private fun BooleanBuilder.filterByIds(ids: List<UUID>?) = ids?.takeIf { it.isNotEmpty() }
        ?.let { and(product.id.contains(it)) }
        ?: this

    private fun BooleanBuilder.filterByTags(ids: List<UUID>?) = ids?.takeIf { it.isNotEmpty() }
        ?.let { and(product.tags.contains(it)) }
        ?: this

    private fun BooleanBuilder.filterByPriceRange(min: BigDecimal?, max: BigDecimal?) = this.apply {
        min?.let { and(product.price.goe(it)) }
        max?.let { and(product.price.loe(it)) }
    }

    private fun BooleanBuilder.filterByValueRange(min: Long?, max: Long?) = this.apply {
        min?.let { and(product.value.goe(it)) }
        max?.let { and(product.value.loe(it)) }
    }
}
