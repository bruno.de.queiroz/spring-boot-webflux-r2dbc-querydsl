package com.example.api.domain.product.dto

enum class UnitType(val value: String) {
    GRAM("g"),
    MILLILITER("ml"),
    LEAF("leaf")
}
