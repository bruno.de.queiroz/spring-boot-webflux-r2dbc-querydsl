package com.example.api.domain.product.dto

import com.example.api.domain.Identifiable
import java.math.BigDecimal
import java.util.UUID

data class ProductTransient(
    override val id: UUID?,
    val slug: String? = null,
    val name: String? = null,
    val description: String? = null,
    val price: BigDecimal? = null,
    val value: Long? = null,
    val unit: UnitType? = null,
    val quantity: Long? = null,
    val tags: List<UUID>? = emptyList()
) : Identifiable<UUID>
