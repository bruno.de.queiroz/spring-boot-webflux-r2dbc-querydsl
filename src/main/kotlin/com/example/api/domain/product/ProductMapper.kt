package com.example.api.domain.product

import com.example.api.domain.ClientMapper
import com.example.api.domain.ServiceMapper
import com.example.api.domain.product.dto.Product
import com.example.api.domain.product.dto.ProductInput
import com.example.api.domain.product.dto.ProductOutput
import com.example.api.domain.product.dto.ProductTransient
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class ProductMapper :
    ClientMapper<ProductInput, ProductOutput, ProductTransient>,
    ServiceMapper<ProductTransient, Product> {

    override fun toOutput(input: ProductTransient) = ProductOutput(
        id = input.id!!,
        slug = input.slug,
        name = input.name,
        description = input.description,
        price = input.price,
        value = input.value,
        unit = input.unit,
        available = input.quantity?.let { it > 0 } ?: false,
        tags = input.tags
    )

    override fun toTransient(input: ProductInput) = ProductTransient(
        id = null,
        slug = input.slug,
        name = input.name,
        description = input.description,
        price = input.price,
        value = input.value,
        unit = input.unit,
        quantity = input.quantity
    )

    override fun asTransient(input: Product) = ProductTransient(
        id = input.id,
        slug = input.slug,
        name = input.name,
        description = input.description,
        price = input.price,
        value = input.value,
        unit = input.unit,
        quantity = input.quantity
    )

    override fun toModel(input: ProductTransient) = Product(
        slug = input.slug!!,
        name = input.name!!,
        description = input.description!!,
        price = input.price!!,
        value = input.value!!,
        unit = input.unit!!,
        quantity = input.quantity!!,
        tags = input.tags ?: emptyList()
    )

    override fun merge(input: ProductTransient, model: Product) = Product(
        id = model.id,
        slug = input.slug ?: model.slug,
        name = input.name ?: model.name,
        description = input.description ?: model.description,
        price = input.price ?: model.price,
        value = input.value ?: model.value,
        unit = input.unit ?: model.unit,
        quantity = input.quantity ?: model.quantity,
        tags = input.tags ?: model.tags,
        createdAt = model.createdAt,
        updatedAt = Instant.now()
    )
}
