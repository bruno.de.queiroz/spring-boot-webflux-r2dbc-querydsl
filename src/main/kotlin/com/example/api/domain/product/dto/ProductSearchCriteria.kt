package com.example.api.domain.product.dto

import org.springframework.web.bind.annotation.RequestParam
import java.math.BigDecimal
import java.util.UUID

data class ProductSearchCriteria(
    @RequestParam(required = false) val text: String?,
    @RequestParam(required = false) val ids: List<UUID>?,
    @RequestParam(required = false) val tags: List<UUID>?,
    @RequestParam(required = false) val minPrice: BigDecimal?,
    @RequestParam(required = false) val maxPrice: BigDecimal?,
    @RequestParam(required = false) val minValue: Long?,
    @RequestParam(required = false) val maxValue: Long?
)
