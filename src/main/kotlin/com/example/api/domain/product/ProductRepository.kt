package com.example.api.domain.product

import com.example.api.domain.CrudRepository
import com.example.api.domain.product.dto.Product
import org.springframework.validation.annotation.Validated
import java.util.UUID

@Validated
interface ProductRepository : CrudRepository<Product, UUID>
