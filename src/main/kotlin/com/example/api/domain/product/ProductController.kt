package com.example.api.domain.product

import com.example.api.domain.CrudController
import com.example.api.domain.product.dto.Product
import com.example.api.domain.product.dto.ProductInput
import com.example.api.domain.product.dto.ProductOutput
import com.example.api.domain.product.dto.ProductSearchCriteria
import com.example.api.domain.product.dto.ProductTransient
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/api/v1/products", produces = [MediaType.APPLICATION_JSON_VALUE])
class ProductController(override val mapper: ProductMapper, override val service: ProductService) :
    CrudController<UUID, ProductInput, ProductOutput, ProductTransient, Product>("Product", mapper, service) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun search(@Valid criteria: ProductSearchCriteria, page: Pageable): Flux<ProductOutput> =
        service.findByCriteria(
            text = criteria.text,
            ids = criteria.ids,
            tags = criteria.tags,
            minPrice = criteria.minPrice,
            maxPrice = criteria.maxPrice,
            minValue = criteria.minValue,
            maxValue = criteria.maxValue,
            page = page
        ).map { mapper.toOutput(it) }
}
