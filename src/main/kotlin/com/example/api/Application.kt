package com.example.api

import com.example.querydsl.SoftDeleteFragmentFactoryBean
import com.infobip.spring.data.r2dbc.R2dbcConfiguration
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.servers.Server
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication
import java.time.ZoneOffset
import java.util.Locale
import java.util.TimeZone

@Import(R2dbcConfiguration::class)
@EnableR2dbcRepositories(repositoryFactoryBeanClass = SoftDeleteFragmentFactoryBean::class)
@SpringBootApplication
@EnableConfigurationProperties
@EnableGlobalAuthentication
@OpenAPIDefinition(servers = [Server(url = "/")])
class Application

fun main(args: Array<String>) {
    Locale.setDefault(Locale.ENGLISH)
    TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC))
    runApplication<Application>(*args)
}
