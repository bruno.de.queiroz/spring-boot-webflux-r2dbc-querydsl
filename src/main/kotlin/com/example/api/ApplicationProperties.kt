package com.example.api

import com.example.api.ApplicationProperties.Companion.PREFIX
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component(PREFIX)
@ConfigurationProperties(PREFIX)
class ApplicationProperties {
    companion object {
        const val PREFIX = "app"
    }

    @Component
    @ConfigurationProperties("$PREFIX.jwt")
    class JwtProperties {
        var secret: String = "fmDa7IyP/Sd6cGEbGSKGcD31eeuD7akQ07lN+FHUR1bBovVfg49AVAYITOj9koMmtYj7kZf05YtgU8FcZdxzRQ=="
        var expiration: Long = 28800L
    }

    @Component
    @ConfigurationProperties("$PREFIX.social-accounts")
    class SocialAccounts {
        var facebook: SocialAccount? = null
        var google: SocialAccount? = null
    }
}

class SocialAccount {
    var clientId: String = "fakeClientId"
    var clientSecret: String = "fakeClientSecret"
}
