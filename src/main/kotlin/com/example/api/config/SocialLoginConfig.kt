package com.example.api.config

import com.example.api.ApplicationProperties
import com.github.scribejava.apis.FacebookApi
import com.github.scribejava.apis.GoogleApi20
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.oauth.OAuth20Service
import com.github.scribejava.httpclient.ning.NingHttpClientConfig
import com.ning.http.client.AsyncHttpClientConfig
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SocialLoginConfig {

    @Bean
    fun facebookOAuthService(socialAccounts: ApplicationProperties.SocialAccounts): OAuth20Service =
        ServiceBuilder(socialAccounts.facebook!!.clientId)
            .apiSecret(socialAccounts.facebook!!.clientSecret)
            .httpClientConfig(ningConfig())
            .build(FacebookApi.instance())

    @Bean
    fun googleOAuthService(socialAccounts: ApplicationProperties.SocialAccounts): OAuth20Service =
        ServiceBuilder(socialAccounts.google!!.clientId)
            .apiSecret(socialAccounts.google!!.clientSecret)
            .httpClientConfig(ningConfig())
            .build(GoogleApi20.instance())

    private fun ningConfig() =
        NingHttpClientConfig(
            AsyncHttpClientConfig.Builder()
                .setMaxConnections(5)
                .setRequestTimeout(10000)
                .setAllowPoolingConnections(false)
                .setPooledConnectionIdleTimeout(1000)
                .setReadTimeout(1000)
                .build()
        )
}
