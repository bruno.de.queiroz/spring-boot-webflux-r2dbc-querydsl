package com.example.api.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
class CORSGlobalConfig : WebFluxConfigurer {

    // FIXME: cannot go live like that
    override fun addCorsMappings(corsRegistry: CorsRegistry) {
        corsRegistry
            .addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("PUT,POST,GET,OPTIONS,DELETE,PATCH")
    }
}
