package com.example.api.config

import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.annotation.TransactionManagementConfigurer
import javax.sql.DataSource

@Configuration
class DatabaseConfig {

    @Primary
    @Bean
    fun dataSource(
        @Value("\${spring.datasource.url}") url: String,
        @Value("\${spring.datasource.username}") username: String,
        @Value("\${spring.datasource.password}") password: String
    ) = DataSourceBuilder.create()
        .url(url)
        .username(username)
        .password(password)
        .build()

    @Bean(initMethod = "migrate")
    fun flyway(dataSource: DataSource): Flyway = Flyway(
        Flyway.configure()
            .baselineOnMigrate(true)
            .dataSource(dataSource)
    )

    /**
     * Because we have jdbc(for flyway) and r2dbc, this will specify which transaction manager
     * the application will use on @Transactional
     */
    @Bean
    fun transactionManagementConfigurer(reactiveTransactionManager: ReactiveTransactionManager) =
        TransactionManagementConfigurer { reactiveTransactionManager }
}
