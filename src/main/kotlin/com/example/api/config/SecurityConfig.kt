package com.example.api.config

import com.example.api.domain.user.auth.TokenGenerationService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import reactor.core.publisher.Mono

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class SecurityConfig {

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder(12)

    @Bean
    fun authenticationManager(tokenGenerationService: TokenGenerationService): ReactiveAuthenticationManager {
        return ReactiveAuthenticationManager { authentication ->
            Mono.just(authentication.credentials.toString())
                .filter { tokenGenerationService.validateToken(it) }
                .map {
                    val token = tokenGenerationService.parseJwtToken(it)!!
                    val authorities = tokenGenerationService.getAuthoritiesFromClaims(token.body)
                    JwtAuthenticationToken(
                        Jwt(
                            it,
                            token.body.issuedAt.toInstant(),
                            token.body.expiration.toInstant(),
                            token.header,
                            token.body
                        ),
                        authorities,
                        token.body.subject
                    )
                }
        }
    }

    @Bean
    fun securityWebFilterChain(
        jwtAuthFilter: AuthenticationWebFilter,
        authenticationManager: ReactiveAuthenticationManager,
        http: ServerHttpSecurity
    ): SecurityWebFilterChain {
        // Disable common features
        http.csrf().disable()
            .formLogin().disable()
            .httpBasic().disable()

        // JWT login
        http
            .addFilterAt(jwtAuthFilter, SecurityWebFiltersOrder.AUTHENTICATION)
            .authorizeExchange()
            .anyExchange().permitAll()

        return http.build()
    }

    @Bean
    fun jwtAuthFilter(jwtAuthManager: ReactiveAuthenticationManager): AuthenticationWebFilter =
        AuthenticationWebFilter(jwtAuthManager).apply {
            setServerAuthenticationConverter {
                Mono.justOrEmpty(it)
                    .map { exchange -> exchange.request.headers.getFirst(HttpHeaders.AUTHORIZATION) ?: "" }
                    .filter { header -> header.startsWith("Bearer ") }
                    .map { header -> header.substring(7) }
                    .map { token -> UsernamePasswordAuthenticationToken(token, token) }
            }
        }
}
