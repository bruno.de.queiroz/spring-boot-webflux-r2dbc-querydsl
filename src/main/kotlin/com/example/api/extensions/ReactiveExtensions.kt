package com.example.api.extensions

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import reactor.core.publisher.Mono
import java.nio.charset.Charset
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

fun <T> Mono<T>.notFoundIfEmpty(message: String): Mono<T> =
    this.switchIfEmpty(
        Mono.error(
            HttpClientErrorException.create(
                message,
                HttpStatus.NOT_FOUND,
                HttpStatus.NOT_FOUND.reasonPhrase,
                org.springframework.http.HttpHeaders.EMPTY,
                byteArrayOf(),
                Charset.defaultCharset()
            )
        )
    )

fun <T> Future<T>.toMono(timeout: Long = 3L, unit: TimeUnit = TimeUnit.SECONDS) =
    Mono.fromFuture(CompletableFuture.supplyAsync { this.get(timeout, unit) })
