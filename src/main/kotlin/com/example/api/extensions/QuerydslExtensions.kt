package com.example.api.extensions

import com.querydsl.core.types.ExpressionUtils
import com.querydsl.core.types.Order
import com.querydsl.core.types.OrderSpecifier
import com.querydsl.core.types.dsl.ComparablePath
import com.querydsl.core.types.dsl.Expressions
import com.querydsl.core.types.dsl.ListPath
import org.springframework.data.domain.Sort
import java.util.UUID

fun ListPath<UUID, ComparablePath<UUID>>.contains(uuids: List<UUID>) =
    Expressions.booleanTemplate("arraycontains({0}, string_to_array({1}, ',')::uuid[]) = true", this, uuids.joinToString(","))

fun ComparablePath<UUID>.contains(uuids: List<UUID>) =
    ExpressionUtils.`in`(Expressions.stringPath(this.metadata), uuids.map { it.toString() })

fun Sort.toOrderSpecifier() =
    this.map {
        OrderSpecifier(Order.valueOf(it.direction.name), Expressions.stringPath(it.property))
    }.toList().toTypedArray()
