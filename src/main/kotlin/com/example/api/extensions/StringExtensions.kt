package com.example.api.extensions

import java.util.UUID

fun String.toUUID(): UUID = UUID.fromString(this)
