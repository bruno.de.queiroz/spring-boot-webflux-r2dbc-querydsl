package com.example.api

import io.mockk.InternalPlatformDsl.toArray
import org.dbunit.dataset.datatype.AbstractDataType
import org.dbunit.dataset.datatype.DataType
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
import org.postgresql.util.PGobject
import java.sql.Date
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Time
import java.sql.Timestamp
import java.sql.Types
import java.util.UUID

/**
 * Required to make DBUnit work properly with the new types of postgres
 */
class CustomDataTypeFactory : PostgresqlDataTypeFactory() {
    override fun createDataType(sqlType: Int, sqlTypeName: String?): DataType {
        return when (sqlTypeName) {
            "_text" -> return ArrayDataType("text")
            "_uuid" -> return ArrayDataType("uuid")
            "_int" -> return ArrayDataType("int")
            else -> super.createDataType(sqlType, sqlTypeName)
        }
    }

    class ArrayDataType(private val subtype: String) :
        AbstractDataType("ARRAY", Types.ARRAY, String::class.java, false) {

        override fun typeCast(obj: Any?): Any {
            when (obj) {
                is String,
                is UUID,
                is Boolean,
                is Number,
                is Time,
                is Date,
                is Timestamp -> listOf(obj.toString())
                is Array<*> -> obj.map { it.toString() }.toArray()
                else -> throw TypeCastException()
            }
            return obj.toString()
        }

        override fun getSqlValue(column: Int, resultSet: ResultSet): Any {
            return resultSet.getString(column)
        }

        override fun setSqlValue(value: Any, column: Int, statement: PreparedStatement) {
            val array: java.sql.Array = statement.connection.createArrayOf(subtype, toArray(value))
            val arrayObj = PGobject()
            arrayObj.type = "$subtype[]"
            statement.setObject(column, arrayObj)
            statement.setArray(column, array)
        }

        private fun toArray(value: Any): Array<Any> {
            val list = emptyArray<Any>()
            if (value is String) {
                var valueStr = value
                if (valueStr.isNotEmpty()) {
                    valueStr = valueStr.replace("[{}]".toRegex(), "")
                    return valueStr.split(",").toTypedArray()
                }
            }
            return list
        }
    }
}
