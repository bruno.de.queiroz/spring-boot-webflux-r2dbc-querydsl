package com.example.api.domain.tag

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.IdOutput
import com.example.api.domain.tag.dto.TagOutput
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.UUID

internal class TagControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private val TAG_1_UUID = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
        private val TAG_2_UUID = "375b46cf-4c25-4c04-9b27-1fe78af50098".toUUID()
        private val TAG_3_UUID = "275b46cf-4c25-4c04-9b27-1fe78af50088".toUUID()
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return OK when listing tags`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(TagOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(3)
        assertThat(response?.map { it.id }).containsSequence(TAG_1_UUID, TAG_2_UUID, TAG_3_UUID)
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return OK when filtering tags by name`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .queryParam("text", "test")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(TagOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(2)
        assertThat(response?.map { it.id }).containsSequence(TAG_1_UUID, TAG_2_UUID)
        assertThat(response?.map { it.id }).doesNotContain(TAG_3_UUID)
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return OK and apply pagination and sorting`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .queryParam("page", 1)
                    .queryParam("size", 2)
                    .queryParam("sort", "name")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isOk
            .expectBodyList(TagOutput::class.java)
            .returnResult()
            .responseBody

        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(1)
        assertThat(response?.map { it.id }).containsSequence(TAG_2_UUID)
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return OK when the tag was found by id`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(TagOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(TAG_1_UUID)
    }

    @Test
    fun `Must return NOT_FOUND when the tag wasn't found by id`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    fun `Must return CREATED when the tag was created successfully`() {
        // given
        val request = TagTestFactory.input()

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is2xxSuccessful
            .expectBody(IdOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
    }

    @Test
    fun `Must return 400 when the tag request doesn't comply with the validation`() {
        // given
        val request = TagTestFactory.input(
            name = null,
            slug = "test-slug"
        )

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(400)
        assertThat(response?.errors).isNotEmpty
        assertThat(response?.errors).containsExactlyInAnyOrder(
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be empty", "name")
        )
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return CONFLICT when the tag wasn't created due to a database constraint violation`() {
        val request = TagTestFactory.input(slug = "test-tag")

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return OK when the tag was updated successfully`() {
        val request = TagTestFactory.input(slug = "test-tag-3")

        // when
        testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_2_UUID")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus()
            .isAccepted

        // then
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_2_UUID")
                    .build()
            }
            .exchange()
            .expectBody(TagOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.slug).isEqualTo("test-tag-3")
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return CONFLICT when the tag wasn't updated due to a database constraint violation`() {
        val request = TagTestFactory.input(slug = "test-tag")

        // when
        val response = testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_2_UUID")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }

    @Test
    fun `Must return NOT_FOUND when the tag wasn't found by id for update`() {
        val randomId = UUID.randomUUID()
        val request = TagTestFactory.input(slug = "non-existing")

        // when
        val response = testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$randomId")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    @DataSet("tags.yml", cleanAfter = true)
    fun `Must return NO_CONTENT when the tag was deleted successfully`() {
        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$TAG_2_UUID")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNoContent
    }

    @Test
    fun `Must return NOT_FOUND when the tag wasn't found by id for delete`() {
        val randomId = UUID.randomUUID()

        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/tags/$randomId")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNotFound
    }
}
