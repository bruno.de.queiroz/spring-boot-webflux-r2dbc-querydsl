package com.example.api.domain.tag

import com.example.api.domain.tag.dto.TagInput

object TagTestFactory {
    fun input(
        name: String? = "Test Category",
        slug: String? = "test-category",
        description: String? = "test description"
    ): TagInput = TagInput(
        name = name,
        slug = slug,
        description = description
    )
}
