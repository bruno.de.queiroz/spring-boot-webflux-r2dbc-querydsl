package com.example.api.domain.product

import com.example.api.AbstractIntegrationTest
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import reactor.kotlin.test.test
import java.util.UUID
import java.util.concurrent.atomic.AtomicReference

internal class ProductRepositoryIntegrationTest : AbstractIntegrationTest() {

    @Autowired
    private lateinit var repository: ProductRepository

    @Autowired
    private lateinit var mapper: ProductMapper

    @Test
    @DataSet(cleanBefore = true)
    fun `Must soft delete the product`() {
        val id = AtomicReference<UUID>()
        val entity = mapper.toModel(mapper.toTransient(ProductTestFactory.input()))

        repository.save(entity)
            .doOnNext { id.set(it.id!!) }
            .flatMap { repository.deactivate(it.id!!) }
            .test()
            .expectComplete()
            .verify()

        // then assert find by id won't find
        repository.findActive(id.get())
            .test()
            .expectComplete()
            .verify()

        // then assert the data is still there
        repository.findById(id.get())
            .test()
            .assertNext {
                assertThat(it.id).isEqualTo(id.get())
                assertThat(it.deletedAt).isNotNull()
            }
            .expectComplete()
            .verify()
    }
}
