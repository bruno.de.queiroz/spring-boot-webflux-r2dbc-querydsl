package com.example.api.domain.product

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.IdOutput
import com.example.api.domain.product.dto.ProductOutput
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.UUID

internal class ProductControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private val PRODUCT_1_UUID = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
        private val PRODUCT_2_UUID = "375b46cf-4c25-4c04-9b27-1fe78af50098".toUUID()
        private val PRODUCT_3_UUID = "275b46cf-4c25-4c04-9b27-1fe78af50088".toUUID()
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when listing products`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(3)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_1_UUID, PRODUCT_2_UUID, PRODUCT_3_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when filtering products by name`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .queryParam("text", "test")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(2)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_1_UUID, PRODUCT_2_UUID)
        assertThat(response?.map { it.id }).doesNotContain(PRODUCT_3_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when filtering products by ids`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .queryParam("ids", listOf(PRODUCT_1_UUID, PRODUCT_3_UUID))
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(2)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_1_UUID, PRODUCT_3_UUID)
        assertThat(response?.map { it.id }).doesNotContain(PRODUCT_2_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when filtering products by minPrice and maxPrice`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .queryParam("minPrice", "0.3")
                    .queryParam("maxPrice", "0.3")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(1)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_2_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when filtering products by minValue and maxValue`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .queryParam("minValue", "500")
                    .queryParam("maxValue", "500")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(1)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_2_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK and apply pagination and sorting`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .queryParam("page", 1)
                    .queryParam("size", 2)
                    .queryParam("sort", "name")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isOk
            .expectBodyList(ProductOutput::class.java)
            .returnResult()
            .responseBody

        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(1)
        assertThat(response?.map { it.id }).containsSequence(PRODUCT_2_UUID)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when the product was found by id`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(PRODUCT_1_UUID)
    }

    @Test
    fun `Must return NOT_FOUND when the product wasn't found by id`() {
        // when
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    fun `Must return CREATED when the product was created successfully`() {
        // given
        val request = ProductTestFactory.input()

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is2xxSuccessful
            .expectBody(IdOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
    }

    @Test
    fun `Must return 400 when the product request doesn't comply with the validation`() {
        // given
        val request = ProductTestFactory.input(
            name = null,
            slug = "test-slug"
        )

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(400)
        assertThat(response?.errors).isNotEmpty
        assertThat(response?.errors).containsExactlyInAnyOrder(
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be empty", "name")
        )
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return CONFLICT when the product wasn't created due to a database constraint violation`() {
        val request = ProductTestFactory.input(slug = "test-product")

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return OK when the product was updated successfully`() {
        val request = ProductTestFactory.input(slug = "test-product-3")

        // when
        testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_2_UUID")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus()
            .isAccepted

        // then
        val response = testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_2_UUID")
                    .build()
            }
            .exchange()
            // then
            .expectBody(ProductOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.slug).isEqualTo("test-product-3")
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return CONFLICT when the product wasn't updated due to a database constraint violation`() {
        val request = ProductTestFactory.input(slug = "test-product")

        // when
        val response = testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_2_UUID")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }

    @Test
    fun `Must return NOT_FOUND when the product wasn't found by id for update`() {
        val randomId = UUID.randomUUID()
        val request = ProductTestFactory.input(name = "non-existing")

        // when
        val response = testClient.put()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$randomId")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    @DataSet("products.yml", cleanAfter = true)
    fun `Must return NO_CONTENT when the product was deleted successfully`() {
        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$PRODUCT_2_UUID")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNoContent
    }

    @Test
    fun `Must return NOT_FOUND when the product wasn't found by id for delete`() {
        val randomId = UUID.randomUUID()

        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/products/$randomId")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNotFound
    }
}
