package com.example.api.domain.product

import com.example.api.domain.product.dto.ProductInput
import com.example.api.domain.product.dto.UnitType
import java.math.BigDecimal
import java.util.UUID

object ProductTestFactory {
    fun input(
        name: String? = "Test Product",
        slug: String? = "test-product",
        description: String? = "test description",
        price: BigDecimal? = BigDecimal(0.1),
        value: Long? = 1000,
        unit: UnitType? = UnitType.MILLILITER,
        quantity: Long? = 1,
        tags: List<UUID>? = emptyList()
    ): ProductInput = ProductInput(
        slug = slug,
        name = name,
        description = description,
        price = price,
        value = value,
        unit = unit,
        quantity = quantity,
        tags = tags
    )
}
