package com.example.api.domain.user.auth

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.user.auth.dto.CredentialInput
import com.example.api.domain.user.auth.dto.TokenInput
import com.example.api.domain.user.auth.dto.TokenOutput
import com.example.api.exception.ErrorResponse
import com.github.database.rider.core.api.dataset.DataSet
import com.github.scribejava.core.model.Response
import io.mockk.every
import org.apache.commons.lang3.concurrent.ConcurrentUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class AuthenticationControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private const val PASSWORD = "whateverpassword"
        private const val FACEBOOK_TOKEN = "EAAQhqGY97rABAD2rzP9m3WWn3pZB385g7DuyT2UtBmSyUPtQ4sPnn3mwWQxxpQt79ri2oqcTv9iZCSdZB3HiZAfejz8PS2YNlcn7OZARZAZBZBfiusynaaOrr43RpjHhc7V3AUIdsimBpgQLCZBzsC7UnZCA7h1ZCowDKwrEA93HC4mPYC89CXj2IGO"
        private const val GOOGLE_TOKEN = "ya29.a0AfH6SMBux5ISavmUUwR47H9ThLY3rFATUffXf0ejRpF2NZV4nrWmYLZ6evFZdn2aU0U9RkcmGuPoACANtg0sXT-PujC65kNdJ6FVmqfsqRtxdqLzVcXXqJehMIKKbdjVLvt2NYQ5jNXxVopHBajOyoE_JXebjQ"
    }

    @Test
    @DataSet("user-verified.yml", cleanAfter = true)
    fun `Must return OK for authenticate`() {
        // given
        val input = CredentialInput(
            email = "test.verified@email.com",
            password = PASSWORD
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus().isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
    }

    @Test
    fun `Must return UNAUTHORIZED for authenticate if user is not found`() {
        // given
        val input = CredentialInput(
            email = "test@email.com",
            password = PASSWORD
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus().isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    @DataSet("user-is-deleted.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for authenticate if user is not enabled`() {
        // given
        val input = CredentialInput(
            email = "test.deleted@email.com",
            password = PASSWORD
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus().isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    @DataSet("user-verified.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for authenticate if user password doesn't match`() {
        // given
        val input = CredentialInput(
            email = "test.verified@email.com",
            password = "abcdeasda"
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus().isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    @DataSet("user-not-verified.yml", cleanAfter = true)
    fun `Must return OK for authenticate if user is not verified but with field verified = false`() {
        // given
        val input = CredentialInput(
            email = "test.not.verified@email.com",
            password = PASSWORD
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
        assertThat(response?.verified).isFalse
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return OK for an existing user if access token is accepted by facebook`() {
        // given
        val input = TokenInput(token = FACEBOOK_TOKEN)

        every { facebookOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(200, "success", emptyMap(), "{\"email\":\"user1@email.com\"}")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/facebook")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
        assertThat(response?.completed).isTrue
        assertThat(response?.verified).isTrue
    }

    @Test
    fun `Must return OK for authenticate if access token is accepted by facebook`() {
        // given
        val input = TokenInput(token = FACEBOOK_TOKEN)

        every { facebookOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(200, "success", emptyMap(), "{\"email\":\"test@email.com\"}")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/facebook")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
        assertThat(response?.completed).isFalse
        assertThat(response?.verified).isFalse
    }

    @Test
    fun `Must return UNAUTHORIZED for authenticate if access token is not accepted by facebook`() {
        // given
        val input = TokenInput(token = FACEBOOK_TOKEN)

        every { facebookOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(400, "error", emptyMap(), "")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/facebook")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    fun `Must return UNAUTHORIZED for authenticate if and error occurs fetching facebook profile`() {
        // given
        val input = TokenInput(token = FACEBOOK_TOKEN)

        every { facebookOAuthService.executeAsync(any()) } throws Exception("any exception")
        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/facebook")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return OK for an existing user if access token is accepted by google`() {
        // given
        val input = TokenInput(token = GOOGLE_TOKEN)

        every { googleOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(200, "success", emptyMap(), "{\"email\":\"user1@email.com\"}")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/google")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
        assertThat(response?.completed).isTrue
        assertThat(response?.verified).isTrue
    }

    @Test
    fun `Must return OK for authenticate if access token is accepted by google`() {
        // given
        val input = TokenInput(token = GOOGLE_TOKEN)

        every { googleOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(200, "success", emptyMap(), "{\"email\":\"test@email.com\"}")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/google")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isOk
            .expectBody(TokenOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.token).isNotBlank
        assertThat(response?.completed).isFalse
        assertThat(response?.verified).isFalse
    }

    @Test
    fun `Must return UNAUTHORIZED for authenticate if access token is not accepted by google`() {
        // given
        val input = TokenInput(token = GOOGLE_TOKEN)

        every { googleOAuthService.executeAsync(any()) } returns ConcurrentUtils.constantFuture(
            Response(400, "error", emptyMap(), "")
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/google")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }

    @Test
    fun `Must return UNAUTHORIZED for authenticate if and error occurs fetching google profile`() {
        // given
        val input = TokenInput(token = GOOGLE_TOKEN)

        every { googleOAuthService.executeAsync(any()) } throws Exception("any exception")

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/auth/google")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus()
            .isUnauthorized
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(401)
    }
}
