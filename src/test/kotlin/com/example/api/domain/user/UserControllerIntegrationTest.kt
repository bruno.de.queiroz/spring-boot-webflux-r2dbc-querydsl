package com.example.api.domain.user

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.IdOutput
import com.example.api.domain.user.dto.UserOutput
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.UUID

internal class UserControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private val USER_1_UUID = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return OK for fetch`() {
        // when
        val response = testClient.get()
            .asCustomer(USER_1_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(UserOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(USER_1_UUID)
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return OK for fetch when accessed by ADMIN`() {
        // when
        val response = testClient.get()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(UserOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(USER_1_UUID)
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return FORBIDDEN for fetch when accessed with different auth user`() {
        // when
        testClient.get()
            .asCustomer()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isForbidden
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return FORBIDDEN for fetch when accessed with a wrong role`() {
        // when
        testClient.get()
            .authorize(UUID.randomUUID(), "OTHER_ROLE")
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isForbidden
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for fetch when there is no token provided`() {
        // when
        testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus()
            .isUnauthorized
    }

    @Test
    fun `Must return NOT_FOUND for fetch when user id want't found`() {
        // when
        val response = testClient.get()
            .asCustomer(USER_1_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    fun `Must return CREATED for create`() {
        // given
        val input = UserTestFactory.input()

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            // then
            .expectStatus().is2xxSuccessful
            .expectBody(IdOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isNotNull
    }

    @Test
    fun `Must return BAD_REQUEST for create when the input doesn't comply with the validation`() {
        // given
        val input = UserTestFactory.input(
            password = null,
            email = "a@b.c"
        )

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            // then
            .expectStatus().isBadRequest
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(400)
        assertThat(response?.errors).isNotEmpty
        assertThat(response?.errors).containsExactlyInAnyOrder(
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be empty", "password"),
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "size must be between 6 and 100", "email")
        )
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return CONFLICT for create due to a database constraint violation`() {
        val input = UserTestFactory.input()

        // when
        val response = testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }
}
