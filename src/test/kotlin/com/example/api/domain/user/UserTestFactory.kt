package com.example.api.domain.user

import com.example.api.domain.user.dto.UserInput

object UserTestFactory {
    fun input(
        email: String? = "user1@email.com",
        password: String? = "someverylongandsafepassword"
    ): UserInput = UserInput(
        email = email,
        password = password
    )
}
