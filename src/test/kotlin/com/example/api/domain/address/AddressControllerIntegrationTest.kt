package com.example.api.domain.address

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.IdOutput
import com.example.api.domain.address.dto.AddressOutput
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.core.ParameterizedTypeReference

internal class AddressControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private val USER_UUID = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
        private val USER_ADDRESSES = listOf(
            "55b93e6f-ab80-4492-a5b1-e36588c10f38".toUUID(),
            "385f71d7-4252-4798-a625-ea6cd37df6be".toUUID()
        )
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return OK on fetch`() {
        // when
        val response = testClient.get()
            .asCustomer(USER_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses/${USER_ADDRESSES[0]}")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(AddressOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(USER_ADDRESSES[0])
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for fetch when there is no token provided`() {
        // when
        testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses/${USER_ADDRESSES[0]}")
                    .build()
            }
            .exchange()
            .expectStatus().isUnauthorized
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return OK on fetchAll`() {
        // when
        val response = testClient.get()
            .asCustomer(USER_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(object : ParameterizedTypeReference<List<AddressOutput>>() {})
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.map { it.id }).containsSequence(USER_ADDRESSES)
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for fetchAll when there is no token provided`() {
        // when
        testClient.get()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses")
                    .build()
            }
            .exchange()
            .expectStatus().isUnauthorized
    }

    @Test
    fun `Must return NOT_FOUND for fetch when the address wasn't found`() {
        // when
        val response = testClient.get()
            .asCustomer(USER_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses/${USER_ADDRESSES[0]}")
                    .build()
            }
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return CREATED for create`() {
        // given
        val input = AddressTestFactory.input()

        // when
        val response = testClient.post()
            .asCustomer(USER_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            // then
            .expectStatus().is2xxSuccessful
            .expectBody(IdOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isNotNull
    }

    @Test
    @DataSet("user-with-addresses.yml", cleanAfter = true)
    fun `Must return UNAUTHORIZED for create when there is no token provided`() {
        // given
        val input = AddressTestFactory.input()

        // when
        testClient.post()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            .expectStatus().isUnauthorized
    }

    @Test
    fun `Must return BAD_REQUEST for create when the input doesn't comply with the validation`() {
        // given
        val input = AddressTestFactory.input(
            alias = null,
            line1 = "abc",
            latitude = null,
            longitude = null
        )

        // when
        val response = testClient.post()
            .asCustomer(USER_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/users/$USER_UUID/addresses")
                    .build()
            }
            .bodyValue(input)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(400)
        assertThat(response?.errors).isNotEmpty
        assertThat(response?.errors).containsExactlyInAnyOrder(
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be null", "latitude"),
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be null", "longitude"),
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be empty", "alias"),
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "size must be between 5 and 255", "line1")
        )
    }
}
