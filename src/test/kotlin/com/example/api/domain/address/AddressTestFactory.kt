package com.example.api.domain.address

import com.example.api.domain.address.dto.AddressInput

object AddressTestFactory {

    fun input(
        firstName: String? = "John",
        lastName: String? = "Doe",
        alias: String? = "work",
        line1: String? = "Ibsenstrasse 62",
        line2: String? = null,
        companyName: String? = null,
        postalCode: String? = "10439",
        latitude: Double? = 0.0,
        longitude: Double? = 0.0,
        district: String? = "Prenzslauer Berg",
        city: String? = "Berlin",
        country: String? = "DEU",
        phoneNumber: String? = "+491794401157"
    ): AddressInput = AddressInput(
        firstName = firstName,
        lastName = lastName,
        alias = alias,
        line1 = line1,
        line2 = line2,
        companyName = companyName,
        postalCode = postalCode,
        latitude = latitude,
        longitude = longitude,
        district = district,
        city = city,
        country = country,
        phoneNumber = phoneNumber
    )
}
