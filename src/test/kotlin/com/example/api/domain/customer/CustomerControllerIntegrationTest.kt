package com.example.api.domain.customer

import com.example.api.AbstractIntegrationTest
import com.example.api.domain.IdOutput
import com.example.api.domain.customer.dto.CustomerOutput
import com.example.api.exception.ErrorCode
import com.example.api.exception.ErrorResponse
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.util.UUID

internal class CustomerControllerIntegrationTest : AbstractIntegrationTest() {

    companion object {
        private val CUSTOMER_1_UUID = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
        private val CUSTOMER_2_UUID = "375b46cf-4c25-4c04-9b27-1fe78af50098".toUUID()
        private val CUSTOMER_3_UUID = "275b46cf-4c25-4c04-9b27-1fe78af50088".toUUID()
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return OK when listing customers`() {
        // when
        val response = testClient.get()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(CustomerOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(3)
        assertThat(response?.map { it.id }).containsExactlyInAnyOrder(CUSTOMER_1_UUID, CUSTOMER_2_UUID, CUSTOMER_3_UUID)
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return OK when filtering customers by name`() {
        // when
        val response = testClient.get()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .queryParam("text", "test")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList(CustomerOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.size).isEqualTo(2)
        assertThat(response?.map { it.id }).containsSequence(CUSTOMER_1_UUID, CUSTOMER_2_UUID)
        assertThat(response?.map { it.id }).doesNotContain(CUSTOMER_3_UUID)
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return OK and apply pagination and sorting`() {
        // when
        val response = testClient.get()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .queryParam("page", 1)
                    .queryParam("size", 2)
                    .queryParam("sort", "first_name")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isOk
            .expectBodyList(CustomerOutput::class.java)
            .returnResult()

        println(response.responseBodyContent)
        val body = response.responseBody
        assertThat(body).isNotNull
        assertThat(body?.size).isEqualTo(1)
        assertThat(body?.map { it.id }).containsSequence(CUSTOMER_2_UUID)
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return OK when the customer was found by id`() {
        // when
        val response = testClient.get()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$CUSTOMER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBody(CustomerOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.id).isEqualTo(CUSTOMER_1_UUID)
    }

    @Test
    fun `Must return NOT_FOUND when the customer wasn't found by id`() {
        // when
        val response = testClient.get()
            .asCustomer(CUSTOMER_1_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$CUSTOMER_1_UUID")
                    .build()
            }
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    @DataSet("user.yml", cleanAfter = true)
    fun `Must return CREATED when the customer was created successfully`() {
        // given
        val request = CustomerTestFactory.input()

        // when
        val response = testClient.post()
            .asCustomer(CUSTOMER_1_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is2xxSuccessful
            .expectBody(IdOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
    }

    @Test
    fun `Must return BAD_REQUEST when the customer request doesn't comply with the validation`() {
        // given
        val request = CustomerTestFactory.input(
            firstName = null,
            birthDate = LocalDate.now()
        )

        // when
        val response = testClient.post()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(400)
        assertThat(response?.errors).isNotEmpty
        assertThat(response?.errors).containsExactlyInAnyOrder(
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must not be empty", "firstName"),
            ErrorResponse.ErrorItem.fieldError(ErrorCode.VALIDATION_ERROR, "must be true", "over18YearsOld")
        )
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return CONFLICT when the customer wasn't created because the user has already a customer account`() {
        val request = CustomerTestFactory.input()

        // when
        val response = testClient.post()
            .asCustomer(CUSTOMER_2_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            // then
            .expectStatus().is4xxClientError
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(409)
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return OK when the customer was updated successfully`() {
        val request = CustomerTestFactory.input(lastName = "Updated")

        // when
        testClient.put()
            .asCustomer(CUSTOMER_2_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$CUSTOMER_2_UUID")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus()
            .isAccepted

        // then
        val response = testClient.get()
            .asCustomer(CUSTOMER_2_UUID)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$CUSTOMER_2_UUID")
                    .build()
            }
            .exchange()
            // then
            .expectBody(CustomerOutput::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.lastName).isEqualTo("Updated")
    }

    @Test
    fun `Must return NOT_FOUND when the customer wasn't found by id for update`() {
        val randomId = UUID.randomUUID()
        val request = CustomerTestFactory.input(firstName = "non-existing")

        // when
        val response = testClient.put()
            .asCustomer(randomId)
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$randomId")
                    .build()
            }
            .bodyValue(request)
            .exchange()
            .expectStatus().isNotFound
            .expectBody(ErrorResponse::class.java)
            .returnResult()
            .responseBody

        // then
        assertThat(response).isNotNull
        assertThat(response?.status).isEqualTo(404)
    }

    @Test
    @DataSet("customers.yml", cleanAfter = true)
    fun `Must return NO_CONTENT when the customer was deleted successfully`() {
        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$CUSTOMER_2_UUID")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNoContent
    }

    @Test
    fun `Must return NOT_FOUND when the customer wasn't found by id for delete`() {
        val randomId = UUID.randomUUID()

        // when
        testClient.delete()
            .asAdmin()
            .uri { uriBuilder ->
                uriBuilder
                    .path("/api/v1/customers/$randomId")
                    .build()
            }
            .exchange()
            // then
            .expectStatus().isNotFound
    }
}
