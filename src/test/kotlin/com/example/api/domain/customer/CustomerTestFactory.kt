package com.example.api.domain.customer

import com.example.api.domain.customer.dto.CustomerInput
import java.time.LocalDate

object CustomerTestFactory {
    fun input(
        firstName: String? = "Test",
        lastName: String? = "Customer",
        birthDate: LocalDate? = LocalDate.of(1983, 2, 12),
        phoneNumber: String? = "+491794401157"
    ): CustomerInput = CustomerInput(
        firstName = firstName,
        lastName = lastName,
        birthDate = birthDate,
        phoneNumber = phoneNumber
    )
}
