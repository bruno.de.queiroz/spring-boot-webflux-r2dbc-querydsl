package com.example.api.domain.customer

import com.example.api.AbstractIntegrationTest
import com.example.api.extensions.toUUID
import com.github.database.rider.core.api.dataset.DataSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import reactor.kotlin.test.test

internal class CustomerRepositoryIntegrationTest : AbstractIntegrationTest() {

    @Autowired
    private lateinit var repository: CustomerRepository

    @Autowired
    private lateinit var mapper: CustomerMapper

    @Test
    @DataSet("user.yml", cleanBefore = true)
    fun `Must soft delete the customer`() {
        val id = "575b46cf-4c25-4c04-9b27-1fe78af50099".toUUID()
        val entity = mapper.toModel(mapper.toTransient(CustomerTestFactory.input()).copy(id = id))

        repository.save(entity)
            .flatMap { repository.deactivate(it.id!!) }
            .test()
            .expectComplete()
            .verify()

        // then assert find by id won't find
        repository.findActive(id)
            .test()
            .expectComplete()
            .verify()

        // then assert the data is still there
        repository.findById(id)
            .test()
            .assertNext {
                assertThat(it.id).isEqualTo(id)
                assertThat(it.deletedAt).isNotNull()
            }
            .expectComplete()
            .verify()
    }
}
