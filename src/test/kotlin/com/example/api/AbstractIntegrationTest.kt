package com.example.api

import com.example.api.domain.user.auth.TokenGenerationService
import com.example.api.domain.user.dto.Role
import com.github.database.rider.junit5.DBUnitExtension
import com.github.scribejava.core.oauth.OAuth20Service
import com.ninjasquad.springmockk.SpykBean
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.WebTestClient.RequestHeadersSpec
import org.springframework.test.web.reactive.server.WebTestClient.RequestHeadersUriSpec
import java.util.UUID

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = [Application::class])
@ExtendWith(value = [SpringExtension::class, DBUnitExtension::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureDataR2dbc
@AutoConfigureWebTestClient
abstract class AbstractIntegrationTest {

    @SpykBean(name = "facebookOAuthService")
    protected lateinit var facebookOAuthService: OAuth20Service

    @SpykBean(name = "googleOAuthService")
    protected lateinit var googleOAuthService: OAuth20Service

    @Autowired
    protected lateinit var testClient: WebTestClient

    @Autowired
    private lateinit var tokenGenerationService: TokenGenerationService

    protected fun generateJwtToken(id: UUID, vararg roles: String): String =
        "Bearer " + tokenGenerationService.generateToken(id, roles.toSet())

    fun <T : RequestHeadersSpec<T>> RequestHeadersUriSpec<T>.authorize(userId: UUID = UUID.randomUUID(), vararg roles: String): RequestHeadersUriSpec<T> =
        this.apply { header("Authorization", generateJwtToken(userId, *roles)) }

    fun <T : RequestHeadersSpec<T>> RequestHeadersUriSpec<T>.asAdmin(userId: UUID = UUID.randomUUID()) =
        this.authorize(userId, Role.ADMIN.name)

    fun <T : RequestHeadersSpec<T>> RequestHeadersUriSpec<T>.asCustomer(userId: UUID = UUID.randomUUID()) =
        this.authorize(userId, Role.CUSTOMER.name)
}
