FROM openjdk:11-jdk-slim
COPY build/libs/api*.jar api.jar
CMD java -XX:+UnlockExperimentalVMOptions -XX:MinRAMPercentage=50 -XX:MaxRAMPercentage=80 -XX:MaxDirectMemorySize=256m -jar api.jar
EXPOSE 8080