import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.4.2"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.4.32"
	kotlin("plugin.spring") version "1.4.32"
	kotlin("kapt") version "1.4.32"
	id("com.diffplug.spotless") version "6.0.4"
}

group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

val jjwtVersion = "0.11.2"
val logstashEncoderVersion = "7.0.1"
val scribeJavaVersion = "8.3.1"
val infoBipQueryDslVersion = "5.4.0"
val springDocVersion = "1.5.13"
val springSocialVersion = "1.1.6.RELEASE"
val dbunitVersion = "2.7.2"
val databaseRiderVersion = "1.32.0"
val springTestDbunitVersion = "1.3.0"
val testcontainersVersion = "1.15.3"
val testContainersSpringBootVersion = "2.0.18"
val mockkVersion = "1.12.1"
val springMockVersion = "3.0.1"
val assertjVersion = "3.21.0"
val springCloudVersion = "2020.0.1"

configurations {
	compileOnly {
		extendsFrom(configurations.kapt.get())
	}

	spotless {
		kotlin {
			target("src/**/*.kt")
			ktlint("0.40.0")
			trimTrailingWhitespace()
			indentWithSpaces()
			endWithNewline()
		}
	}
}

dependencies {
	kapt("org.springframework.boot:spring-boot-configuration-processor")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("org.springframework.cloud:spring-cloud-starter-circuitbreaker-reactor-resilience4j")
	implementation("org.flywaydb:flyway-core")
	implementation("org.json:json:20211205")
	implementation("org.passay:passay:1.6.1")
	runtimeOnly("io.r2dbc:r2dbc-postgresql")
	implementation("org.postgresql:postgresql")
	implementation("io.jsonwebtoken:jjwt-api:$jjwtVersion")
	runtimeOnly("io.jsonwebtoken:jjwt-impl:$jjwtVersion")
	runtimeOnly("io.jsonwebtoken:jjwt-jackson:$jjwtVersion")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("com.github.scribejava:scribejava-apis:$scribeJavaVersion")
	implementation("com.github.scribejava:scribejava-httpclient-ning:$scribeJavaVersion")
	implementation("net.logstash.logback:logstash-logback-encoder:$logstashEncoderVersion")
	implementation("org.springdoc:springdoc-openapi-webflux-ui:$springDocVersion")
	implementation("com.infobip:infobip-spring-data-r2dbc-querydsl:$infoBipQueryDslVersion")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
		exclude(module = "mockito-core")
	}
	testImplementation("commons-collections:commons-collections:3.2.2")
	testImplementation("org.dbunit:dbunit:$dbunitVersion")
	testImplementation("com.github.database-rider:rider-core:$databaseRiderVersion")
	testImplementation("com.github.database-rider:rider-junit5:$databaseRiderVersion")
	testImplementation("com.github.springtestdbunit:spring-test-dbunit:$springTestDbunitVersion")
	testImplementation("io.projectreactor:reactor-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.testcontainers:r2dbc")
	testImplementation("com.playtika.testcontainers:embedded-postgresql:$testContainersSpringBootVersion")
	testImplementation("org.springframework.cloud:spring-cloud-starter")
	testImplementation("org.springframework.cloud:spring-cloud-starter-bootstrap")
	testImplementation("io.mockk:mockk:$mockkVersion")
	testImplementation("com.ninja-squad:springmockk:$springMockVersion")
	testImplementation("org.assertj:assertj-core:$assertjVersion")
}

dependencyManagement {
	imports {
		mavenBom("org.testcontainers:testcontainers-bom:${testcontainersVersion}")
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}")
	}
}

tasks {
	withType<KotlinCompile> {
		dependsOn(spotlessApply)
		dependsOn(spotlessCheck)
		kotlinOptions {
			freeCompilerArgs = listOf("-Xjsr305=strict")
			javaParameters = true
			jvmTarget = "11"
		}
	}

	withType<Test> {
		useJUnitPlatform()
		failFast = true
		testLogging {
			lifecycle {
				events = mutableSetOf(TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED)
				exceptionFormat = TestExceptionFormat.FULL
				showExceptions = true
				showCauses = true
				showStackTraces = true
				showStandardStreams = true
			}
			info.events = lifecycle.events
			info.exceptionFormat = lifecycle.exceptionFormat
		}

		val failedTests = mutableListOf<TestDescriptor>()
		val skippedTests = mutableListOf<TestDescriptor>()

		// See https://github.com/gradle/kotlin-dsl/issues/836
		addTestListener(object : TestListener {
			override fun beforeSuite(suite: TestDescriptor) {}
			override fun beforeTest(testDescriptor: TestDescriptor) {}
			override fun afterTest(testDescriptor: TestDescriptor, result: TestResult) {
				when (result.resultType) {
					TestResult.ResultType.FAILURE -> failedTests.add(testDescriptor)
					TestResult.ResultType.SKIPPED -> skippedTests.add(testDescriptor)
					else -> Unit
				}
			}

			override fun afterSuite(suite: TestDescriptor, result: TestResult) {
				if (suite.parent == null) { // root suite
					logger.lifecycle("----")
					logger.lifecycle("Test result: ${result.resultType}")
					logger.lifecycle(
						"Test summary: ${result.testCount} tests, " +
							"${result.successfulTestCount} succeeded, " +
							"${result.failedTestCount} failed, " +
							"${result.skippedTestCount} skipped")
					failedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tFailed Tests")
					skippedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tSkipped Tests:")
				}
			}

			private infix fun List<TestDescriptor>.prefixedSummary(subject: String) {
				logger.lifecycle(subject)
				forEach { test -> logger.lifecycle("\t\t${test.displayName()}") }
			}

			private fun TestDescriptor.displayName() = parent?.let { "${it.name} - $name" } ?: name
		})
	}
}
